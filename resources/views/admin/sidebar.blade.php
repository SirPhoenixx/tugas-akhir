  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="{{ asset('lte/dist/img/narada.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">NARADA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      @if (request()->session()->get('id_group') == 1 || request()->session()->get('id_group') == 2)

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
          <a href="/dash" class="nav-link {{ set_active('master.dashboard')}}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview {{ set_menu_open(['manajemen.user', 'manajemen.group', 'manajemen.master']) }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Management Apps
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/manajemenuser" class="nav-link {{ set_active('manajemen.user')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Users</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/manajemengroup" class="nav-link {{ set_active('manajemen.group')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Groups User</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/manajemenmaster" class="nav-link {{ set_active('manajemen.master')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Master Employee</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview {{ set_menu_open(['master.pg', 'master.posll', 'master.kk', 'master.waktu']) }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-plus-square"></i>
              <p>
                Master
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/masterpg" class="nav-link {{ set_active('master.pg')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Master PG</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/masterposll" class="nav-link {{ set_active('master.posll')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Master Pos LL</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/masterkk" class="nav-link {{ set_active('master.kk')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Master KK</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/masterwaktu" class="nav-link {{ set_active('master.waktu')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Waktu Tempuh</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview {{ set_menu_open(['transaksi.masukpg', 'transaksi.masukll']) }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Transaksi
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/transaksimasukll" class="nav-link {{ set_active('transaksi.masukll')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Trip Tebu Masuk Pos LL</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/transaksimasukpg" class="nav-link {{ set_active('transaksi.masukpg')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Trip Tebu Masuk PG</p>
                </a>
              </li>
            </ul>
          </li>

          
          <li class="nav-item has-treeview {{ set_menu_open(['laporan.lapmasukpg', 'laporan.lapgrafiktrip', 'laporan.lapevaluasi', 'laporan.lapmasukposll', 'laporan.lapsemua', 'laporan.lapanalisatrafik', 'laporan.lapkontrak', 'laporan.laptransfer']) }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Laporan
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              <li class="nav-item">
                <a href="/lapmasukposll" class="nav-link {{ set_active('laporan.lapmasukposll')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Trip Masuk Pos LL</p>
                </a>
              </li>
              
              <li class="nav-item">
                <a href="/lapmasukpg" class="nav-link {{ set_active('laporan.lapmasukpg')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Trip Masuk PG</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/lapsemua" class="nav-link {{ set_active('laporan.lapsemua')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Trip Semua Pos</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/lapgrafiktrip" class="nav-link {{ set_active('laporan.lapgrafiktrip')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Grafik Trip</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/lapevaluasi" class="nav-link {{ set_active('laporan.lapevaluasi')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Evaluasi</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/lapkontrak" class="nav-link {{ set_active('laporan.lapkontrak')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kontrak</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/laptransfer" class="nav-link {{ set_active('laporan.laptransfer')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Transfer</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      @elseif(request()->session()->get('id_group') == 3)
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
          <a href="/dash" class="nav-link {{ set_active('master.dashboard')}}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview {{ set_menu_open(['transaksi.masukpg', 'transaksi.masukll']) }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Transaksi
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/transaksimasukpg" class="nav-link {{ set_active('transaksi.masukpg')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Trip Tebu Masuk PG</p>
                </a>
              </li>
            </ul>
          </li>
          
          <li class="nav-item has-treeview {{ set_menu_open(['laporan.lapmasukpg', 'laporan.lapgrafiktrip', 'laporan.lapevaluasi', 'laporan.lapmasukposll', 'laporan.lapsemua', 'laporan.lapanalisatrafik', 'laporan.lapkontrak', 'laporan.laptransfer']) }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Laporan
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <a href="/lapmasukpg" class="nav-link {{ set_active('laporan.lapmasukpg')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Trip Masuk PG</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/lapsemua" class="nav-link {{ set_active('laporan.lapsemua')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Trip Semua Pos</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/lapgrafiktrip" class="nav-link {{ set_active('laporan.lapgrafiktrip')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Grafik Trip</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/lapkontrak" class="nav-link {{ set_active('laporan.lapkontrak')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kontrak</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/laptransfer" class="nav-link {{ set_active('laporan.laptransfer')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Transfer</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      @elseif(request()->session()->get('id_group') == 4)
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
          <a href="/dash" class="nav-link {{ set_active('master.dashboard')}}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview {{ set_menu_open(['transaksi.masukpg', 'transaksi.masukll']) }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Transaksi
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/transaksimasukll" class="nav-link {{ set_active('transaksi.masukll')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Trip Tebu Masuk Pos LL</p>
                </a>
              </li>
            </ul>
          </li>

          
          <li class="nav-item has-treeview {{ set_menu_open(['laporan.lapmasukpg', 'laporan.lapgrafiktrip', 'laporan.lapevaluasi', 'laporan.lapmasukposll', 'laporan.lapsemua', 'laporan.lapanalisatrafik', 'laporan.lapkontrak', 'laporan.laptransfer']) }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Laporan
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              <li class="nav-item">
                <a href="/lapmasukposll" class="nav-link {{ set_active('laporan.lapmasukposll')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Trip Masuk Pos LL</p>
                </a>
              </li>
              
              <li class="nav-item">
                <a href="/lapsemua" class="nav-link {{ set_active('laporan.lapsemua')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Trip Semua Pos</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/lapgrafiktrip" class="nav-link {{ set_active('laporan.lapgrafiktrip')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Grafik Trip</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/lapkontrak" class="nav-link {{ set_active('laporan.lapkontrak')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kontrak</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/laptransfer" class="nav-link {{ set_active('laporan.laptransfer')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Transfer</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      @else
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
          <a href="/dash" class="nav-link {{ set_active('master.dashboard')}}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview {{ set_menu_open(['master.pg', 'master.posll', 'master.kk', 'master.waktu']) }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-plus-square"></i>
              <p>
                Master
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/masterwaktu" class="nav-link {{ set_active('master.waktu')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Waktu Tempuh</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview {{ set_menu_open(['laporan.lapmasukpg', 'laporan.lapgrafiktrip', 'laporan.lapevaluasi', 'laporan.lapmasukposll', 'laporan.lapsemua', 'laporan.lapanalisatrafik', 'laporan.lapkontrak', 'laporan.laptransfer']) }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Laporan
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              <li class="nav-item">
                <a href="/lapmasukpg" class="nav-link {{ set_active('laporan.lapmasukpg')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Trip Masuk PG</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/lapmasukposll" class="nav-link {{ set_active('laporan.lapmasukposll')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Trip Masuk Pos LL</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/lapsemua" class="nav-link {{ set_active('laporan.lapsemua')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Trip Semua Pos</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/lapgrafiktrip" class="nav-link {{ set_active('laporan.lapgrafiktrip')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Grafik Trip</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/lapevaluasi" class="nav-link {{ set_active('laporan.lapevaluasi')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Evaluasi</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/lapkontrak" class="nav-link {{ set_active('laporan.lapkontrak')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kontrak</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/laptransfer" class="nav-link {{ set_active('laporan.laptransfer')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Transfer</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      @endif
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>