@extends('admin.admin')
@section('content')

<script type="text/javascript" src="chartjs/Chart.js"></script>   
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Welcome {{Session::get('nama')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    {{-- year --}}
    {{-- <div class="card-body">
      <div class="row">
        <div class="form-group col-md-12">
          <select name="inputpg" id="inputpg" class="form-control">
            <option value="">Pilih Tahun</option>
            @foreach ($years as $y)
              <option value="{{$y}}">{{$y}}</option>
            @endforeach
          </select>
        </div>
      </div> --}}

    <!-- Main content -->
    <section class="content">
      <!-- 3 Box atas -->
      <div class="row">

        <div class="col-lg-4 col-6">
          <div class="small-box bg-info">
            <div class="inner">
              <h3>{{$trip_today}}</h3>
    
              <p>Trip Hari Ini</p>
              <h5>(Semua Pabrik Gula)</h5>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
          </div>
        </div>  
    
        <div class="col-lg-4 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3>{{$trip_all}}</h3>
    
              <p>Trip Sampai Dengan Hari Ini</p>
              <h5>(Semua Pabrik Gula)</h5>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
          </div>
        </div>
  
        <div class="col-lg-4 col-6">
          <!-- small box -->
          <div class="small-box bg-warning">
            <div class="inner">
              <h3>{{$tripposll_today}}</h3>    
              <p>Truk Masuk Pos LL Hari Ini</p>
              <h5>(Semua Pos Legalisasi</h5>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
          </div>
        </div>
      </div>
      <!-- /3 Box atas -->

      <!-- Default box -->
      <div class="card card-danger pb-4 pl-3">
        <div class="card-header">
          <h3 class="card-title">Chart</h3>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="card-body">
              <div id="chartPg"></div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card-body">
              <div id="chartPosll"></div>
            </div>
          </div>  
        </div>

        
        <div class="row col-md-12">
          <div class="card col-md-6">
            <div class="card-header">
              <h3 class="card-title">Transaksi per PG</h3>
            </div>
          <div class="card-body table-responsive p-0" style="height: 500px">
            <table class="table table-head-fixed table-bordered">
              <thead>
                <tr>
                  <th>Nama PG</th>
                  <th>hari ini</th>
                  <th>sd hari ini</th>
                </tr>
              </thead>
              @foreach ($categoriespg as $bodypg)
              <tbody>
                <td>{{$bodypg}}</td>
                <td>{{$todaypg[$bodypg]}}</td>
                <td>{{$sdtodaypg[$bodypg]}}</td>
              </tbody>
              @endforeach
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
          <div class="card col-md-6">
            <div class="card-header">
              <h3 class="card-title">Transaksi per Pos LL</h3>
            </div>
            <div class="card-body table-responsive p-0" style="height: 500px">
              <table class="table table-head-fixed table-bordered">
                <thead>
                  <tr>  
                    <th>Nama Pos LL</th>
                    <th>hari ini</th>
                    <th>sd hari ini</th>
                  </tr>
                </thead>
                @foreach ($categoriesll as $bodyposll) 
                <tbody>
                  <td>{{$bodyposll}}</td>
                  <td>{{$todayposll[$bodyposll]}}</td>
                  <td>{{$sdtodayposll[$bodyposll]}}</td>
                </tbody>
                @endforeach
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <table class="table table-striped table-bordered table-list" id="dashboard" style="width:100%">
            <thead>
                <tr role="row">
                    <th>No</th>
                    <th>PG Masuk</th>
                    <th>Pos LL</th>
                    <th>Plat Truk</th>
                    <th>No KK</th>
                    <th>Tgl masuk LL</th>
                    <th>Tgl masuk PG</th>
                </tr>
            </thead>
          </table>
          <div class="col-md-12 text-center pt-2">
            <a href="/lapsemua">
            <button>More</button>
            </a>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
    <!-- /.content -->
  </div>
  @endsection
  @section('js')
  <!-- Pastikan posisi dibawah import datatablesnya -->
  <script type="text/javascript">
      $(document).ready(function(){
      $('#dashboard').DataTable({
              processing: false,
              serverSide: true,
              dom: '<"html5buttons">',
              ajax: {
                url: '{{ url("lapsemua") }}'
              },
              order: [[ 6, "desc" ]],
              columns: [
              {data: 'DT_RowIndex',     name: 'DT_RowIndex', orderable: false, searchable: false},  
              {data: 'nama_pgb',   name: 'id_pgmasuk', orderable: false, searchable: false},
              {data: 'nama_posll',     name: 'id_posll', orderable: false, searchable: false},
              {data: 'plat_truk_pg', name: 'plat_truk', orderable: false, searchable: false},
              {data: 'nama_kkb',  name: 'id_kontrak', orderable: false, searchable: false},
              {data: 'tanggal_posll', name: 'tanggal_posll', orderable: false, searchable: false},
              {data: 'tanggal_pg', name: 'tanggal_pg', orderable: true, searchable: false}
              ],
              columnDefs:[{targets:[6, 5], render:function(data){
                  return moment(data).format('LLL');
              }}]
              
          });
        });
  </script>
  @endsection
  @section('chartdash')
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script>
    Highcharts.chart('chartPg', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Data Trip per PG'
    },
    xAxis: {
        categories: {!!json_encode($categoriespg)!!},
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah Trip'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Trip',
        data: {!!json_encode($datapg)!!}

    }]
});
  </script>
  <script>
    Highcharts.chart('chartPosll', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Data Trip per Pos LL'
    },
    xAxis: {
        categories: {!!json_encode($categoriesll)!!},
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah Trip'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Trip',
        data: {!!json_encode($dataposll)!!}

    }]
});
  </script>
  @endsection
