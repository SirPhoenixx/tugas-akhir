@extends('admin.admin')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit KK</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dash">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/masterkk">Master KK</a></li>
              <li class="breadcrumb-item active">Edit KK</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="card card-info">
        <div class="card-header">
        <h3 class="card-title">Detail KK</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form class="form-horizontal" action="/updatekk" method="post">
        {{ csrf_field() }}
        <div class="card-body">
            <input type="hidden" name="inputid" value="{{ $kk->id }}">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">No Kontrak</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $kk->nokontrak }}" class="form-control" name="inputnokontrak" placeholder="No Kontrak">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nama KK</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $kk->nama }}" class="form-control" name="inputnama" placeholder="Nama KK">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">PG</label>
                <div class="col-sm-10">
                <select name="inputpg" class="form-control">
                  @foreach ($pg as $q)
                  <option 
                  @if( $kk->id_pg == $q->id ) selected="selected" @endif 
                  value={{ $q->id }}>{{$q->nama}}
                  </option>    
                  @endforeach
                </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Tahun</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $kk->tahun }}" class="form-control" name="inputtahun" placeholder="Tahun">
                </div>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-info float-right">Save</button>
            <a href="{{url('/hapuskk')}}/{{$kk->id}}">
              <button type="button" class="btn btn-danger float-right">Delete</button>
            </a>
            <a href="{{url('/masterkk')}}">
              <button type="button" class="btn btn-default float-right"> Back to List </button>
            </a>
        </div>
        <!-- /.card-footer -->
        </form>
    </div>
</section>
    <!-- /.content -->
</div>
@endsection