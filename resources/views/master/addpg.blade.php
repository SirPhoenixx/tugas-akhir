@extends('admin.admin')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add PG</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dash">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/masterpg">Master PG</a></li>
              <li class="breadcrumb-item active">Add PG</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="card card-info">
        <div class="card-header">
        <h3 class="card-title">Detail PG</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form class="form-horizontal" action="/addedpg" method="post">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Kode PG</label>
                <div class="col-sm-10">
                    <input type="text" required="required" class="form-control" name="inputkodepg">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nama PG</label>
                <div class="col-sm-10">
                    <input type="text" required="required" class="form-control" name="inputnama">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Lokasi PG</label>
                <div class="col-sm-10">
                    <input type="text" required="required" class="form-control" name="inputlokasi">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Email PG</label>
                <div class="col-sm-10">
                    <input type="email" required="required" class="form-control" name="inputemailpg">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Kota</label>
                <div class="col-sm-10">
                    <select class="custom-select" name="inputkota">
                        <option value="" selected="selected" disabled>Pilih Kota</option>
                        <option value="Bangkalan" name="inputkota">Bangkalan</option>
                        <option value="Banyuwangi" name="inputkota">Banyuwangi</option>
                        <option value="Blitar" name="inputkota">Blitar</option>
                        <option value="Bojonegoro" name="inputkota">Bojonegoro</option>
                        <option value="Bondowoso" name="inputkota">Bondowoso</option>
                        <option value="Gresik" name="inputkota">Gresik</option>
                        <option value="Jember" name="inputkota">Jember</option>
                        <option value="Jombang" name="inputkota">Jombang</option>
                        <option value="Kediri" name="inputkota">Kediri</option>
                        <option value="Lamongan" name="inputkota">Lamongan</option>
                        <option value="Lumajang" name="inputkota">Lumajang</option>
                        <option value="Madiun" name="inputkota">Madiun</option>
                        <option value="Magetan" name="inputkota">Magetan</option>
                        <option value="Malang" name="inputkota">Malang</option>
                        <option value="Mojokerto" name="inputkota">Mojokerto</option>
                        <option value="Nganjuk" name="inputkota">Nganjuk</option>
                        <option value="Ngawi" name="inputkota">Ngawi</option>
                        <option value="Pacitan" name="inputkota">Pacitan</option>
                        <option value="Pamekasan" name="inputkota">Pamekasan</option>
                        <option value="Pasuruan" name="inputkota">Pasuruan</option>
                        <option value="Ponorogo" name="inputkota">Ponorogo</option>
                        <option value="Probolinggo" name="inputkota">Probolinggo</option>
                        <option value="Sampang" name="inputkota">Sampang</option>
                        <option value="Sidoarjo" name="inputkota">Sidoarjo</option>
                        <option value="Situbondo" name="inputkota">Situbondo</option>
                        <option value="Sumenep" name="inputkota">Sumenep</option>
                        <option value="Trenggalek" name="inputkota">Trenggalek</option>
                        <option value="Tuban" name="inputkota">Tuban</option>
                        <option value="Tulungagung" name="inputkota">Tulungagung</option>
                        <option value="Kota Batu" name="inputkota">Kota Batu</option>
                        <option value="Kota Blitar" name="inputkota">Kota Blitar</option>
                        <option value="Kota Kediri" name="inputkota">Kota Kediri</option>
                        <option value="Kota Madiun" name="inputkota">Kota Madiun</option>
                        <option value="Kota Malang" name="inputkota">Kota Malang</option>
                        <option value="Kota Mojokerto" name="inputkota">Kota Mojokerto</option>
                        <option value="Kota Pasuruan" name="inputkota">Kota Pasuruan</option>
                        <option value="Kota Probolinggo" name="inputkota">Kota Probolinggo</option>
                        <option value="Kota Surabaya" name="inputkota">Kota Surabaya</option>
                    </select>
                </div>
            </div>
            <!-- /.checkbox -->
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Unit</label>
                <div class="col-sm-10">
                    <select class="custom-select" name="inputunit">
                        <option value="" selected="selected" disabled>Pilih Unit</option>
                        <option value="Kantor Pusat" name="inputunit">Kantor Pusat</option>
                        <option value="Pabrik Gula" name="inputunit">Pabrik Gula</option>
                    </select>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-info float-right">Save</button>
            <a href="{{url('/masterpg')}}">
                <button type="button" class="btn btn-default float-right"> Back to List </button>
              </a>
        </div>
        <!-- /.card-footer -->
        </form>
    </div>
</section>
    <!-- /.content -->
</div>
@endsection