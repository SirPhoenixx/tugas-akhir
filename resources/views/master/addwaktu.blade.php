@extends('admin.admin')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Waktu</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dash">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/masterwaktu">Master Waktu</a></li>
              <li class="breadcrumb-item active">Add Waktu</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="card card-info">
        <div class="card-header">
        <h3 class="card-title">Detail Waktu</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form class="form-horizontal" action="/addedwaktu" method="post">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">PG</label>
                <div class="col-sm-10">
                <select name="inputpg" class="form-control">
                  <option value="" selected="selected" disabled>Pilih PG</option>
                  @foreach ($pg as $p)
                  <option 
                  value={{ $p->id }}>{{$p->nama}}
                  </option>    
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Pos LL</label>
                <div class="col-sm-10">
                <select name="inputposll" class="form-control">
                  <option value="" selected="selected" disabled>Pilih Pos LL</option>
                  @foreach ($posll as $q)
                  <option 
                  value={{ $q->id }}>{{$q->nama}}
                  </option>    
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nilai</label>
                <div class="col-sm-10">
                    <input type="text" required="required"class="form-control" name="inputnilai">
                </div>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <button type="submit" class="btn btn-info float-right">Save</button>
          <a href="{{url('/masterwaktu')}}">
            <button type="button" class="btn btn-default float-right"> Back to List </button>
          </a>
        </div>
        <!-- /.card-footer -->
        </form>
    </div>
</section>
    <!-- /.content -->
</div>
@endsection