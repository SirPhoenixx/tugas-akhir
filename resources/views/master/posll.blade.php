@extends('admin.admin')
@section('content')
    
<div class="content-wrapper" style="min-height: 1200.88px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">  
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dash">Dashboard</a></li>
              <li class="breadcrumb-item active">Master Pos LL</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content class="text-center" -->
    <section class="content">
      <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Master Pos LL</h3>
                    <div class="actions">
                      <a href="/addposll" class="btn btn-default btn-xs float-right">Add  <i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="card-body">
                  <table class="table table-striped table-bordered table-list" id="tampil-posll" style="width:100%">
                    <thead>
                        <tr role="row">
                            <th>Kode Pos LL</th>
                            <th>Nama Pos LL</th>
                            <th>Lokasi Pos LL</th>
                            <th>Asal Pos LL</th>
                            <th>Updated at</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
                </div>
              </div>
              <!-- /.card -->
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('js')
<!-- Pastikan posisi dibawah import datatablesnya -->
<script type="text/javascript">
    $(function() {
        var oTable = $('#tampil-posll').DataTable({
            processing: false,
            serverSide: true,
            ajax: {
                url: '{{ url("tampilposll") }}'
            },
            columns: [
            {data: 'kodepos',   name: 'kodepos'},
            {data: 'nama',     name: 'nama'},
            {data: 'alamat',   name: 'alamat',  orderable: false},
            {data: 'asalpos', name: 'asalpos',  orderable: false},
            {data: 'updated_at',  name: 'updated_at',   orderable: false, searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            columnDefs:[{targets:4, render:function(data){
                return moment(data).format('LLL');
            }}]
        });
    });
</script>
@endsection
  
  