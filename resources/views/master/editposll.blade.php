@extends('admin.admin')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Pos LL</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dash">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/masterposll">Master Pos LL</a></li>
              <li class="breadcrumb-item active">Edit Pos LL</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="card card-info">
        <div class="card-header">
        <h3 class="card-title">Detail Pos LL</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @foreach($posll as $p)
        <form class="form-horizontal" action="/updateposll" method="post">
        {{ csrf_field() }}
        <div class="card-body">
            <input type="hidden" name="inputid" value="{{ $p->id }}">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Kode Pos LL</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $p->kodepos }}" class="form-control" name="inputkodeposll" placeholder="Kode Pos LL">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nama Pos LL</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $p->nama }}" class="form-control" name="inputnama" placeholder="Nama Pos LL">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Lokasi Pos LL</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $p->alamat }}" class="form-control" name="inputlokasi" placeholder="Lokasi Pos LL">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Kota</label>
                <div class="col-sm-10">
                    <select class="custom-select" name="inputkota">
                        <option value="" selected="selected" disabled>Pilih Kota</option>
                        <option @if($p->kota=="Bangkalan") selected="selected" @endif value="Bangkalan" name="inputkota">Bangkalan</option>
                        <option @if($p->kota=="Banyuwangi") selected="selected" @endif value="Banyuwangi" name="inputkota">Banyuwangi</option>
                        <option @if($p->kota=="Blitar") selected="selected" @endif value="Blitar" name="inputkota">Blitar</option>
                        <option @if($p->kota=="Blora") selected="selected" @endif value="Blora" name="inputkota">Blora</option>                        
                        <option @if($p->kota=="Bojonegoro") selected="selected" @endif value="Bojonegoro" name="inputkota">Bojonegoro</option>
                        <option @if($p->kota=="Bondowoso") selected="selected" @endif value="Bondowoso" name="inputkota">Bondowoso</option>
                        <option @if($p->kota=="Gresik") selected="selected" @endif value="Gresik" name="inputkota">Gresik</option>
                        <option @if($p->kota=="Jember") selected="selected" @endif value="Jember" name="inputkota">Jember</option>
                        <option @if($p->kota=="Jombang") selected="selected" @endif value="Jombang" name="inputkota">Jombang</option>
                        <option @if($p->kota=="Kediri") selected="selected" @endif value="Kediri" name="inputkota">Kediri</option>
                        <option @if($p->kota=="Lamongan") selected="selected" @endif value="Lamongan" name="inputkota">Lamongan</option>
                        <option @if($p->kota=="Lumajang") selected="selected" @endif value="Lumajang" name="inputkota">Lumajang</option>
                        <option @if($p->kota=="Madiun") selected="selected" @endif value="Madiun" name="inputkota">Madiun</option>
                        <option @if($p->kota=="Magetan") selected="selected" @endif value="Magetan" name="inputkota">Magetan</option>
                        <option @if($p->kota=="Malang") selected="selected" @endif value="Malang" name="inputkota">Malang</option>
                        <option @if($p->kota=="Mojokerto") selected="selected" @endif value="Mojokerto" name="inputkota">Mojokerto</option>
                        <option @if($p->kota=="Nganjuk") selected="selected" @endif value="Nganjuk" name="inputkota">Nganjuk</option>
                        <option @if($p->kota=="Ngawi") selected="selected" @endif value="Ngawi" name="inputkota">Ngawi</option>
                        <option @if($p->kota=="Pacitan") selected="selected" @endif value="Pacitan" name="inputkota">Pacitan</option>
                        <option @if($p->kota=="Pamekasan") selected="selected" @endif value="Pamekasan" name="inputkota">Pamekasan</option>
                        <option @if($p->kota=="Pasuruan") selected="selected" @endif value="Pasuruan" name="inputkota">Pasuruan</option>
                        <option @if($p->kota=="Ponorogo") selected="selected" @endif value="Ponorogo" name="inputkota">Ponorogo</option>
                        <option @if($p->kota=="Probolinggo") selected="selected" @endif value="Probolinggo" name="inputkota">Probolinggo</option>
                        <option @if($p->kota=="Rembang") selected="selected" @endif value="Rembang" name="inputkota">Rembang</option>
                        <option @if($p->kota=="Sampang") selected="selected" @endif value="Sampang" name="inputkota">Sampang</option>
                        <option @if($p->kota=="Sidoarjo") selected="selected" @endif value="Sidoarjo" name="inputkota">Sidoarjo</option>
                        <option @if($p->kota=="Situbondo") selected="selected" @endif value="Situbondo" name="inputkota">Situbondo</option>
                        <option @if($p->kota=="Sragen") selected="selected" @endif value="Sragen" name="inputkota">Sragen</option>
                        <option @if($p->kota=="Sumenep") selected="selected" @endif value="Sumenep" name="inputkota">Sumenep</option>
                        <option @if($p->kota=="Trenggalek") selected="selected" @endif value="Trenggalek" name="inputkota">Trenggalek</option>
                        <option @if($p->kota=="Tuban") selected="selected" @endif value="Tuban" name="inputkota">Tuban</option>
                        <option @if($p->kota=="Tulungagung") selected="selected" @endif value="Tulungagung" name="inputkota">Tulungagung</option>
                        <option @if($p->kota=="Kota Batu") selected="selected" @endif value="Kota Batu" name="inputkota">Kota Batu</option>
                        <option @if($p->kota=="Kota Blitar") selected="selected" @endif value="Kota Blitar" name="inputkota">Kota Blitar</option>
                        <option @if($p->kota=="Kota Kediri") selected="selected" @endif value="Kota Kediri" name="inputkota">Kota Kediri</option>
                        <option @if($p->kota=="Kota Madiun") selected="selected" @endif value="Kota Madiun" name="inputkota">Kota Madiun</option>
                        <option @if($p->kota=="Kota Malang") selected="selected" @endif value="Kota Malang" name="inputkota">Kota Malang</option>
                        <option @if($p->kota=="Kota Mojokerto") selected="selected" @endif value="Kota Mojokerto" name="inputkota">Kota Mojokerto</option>
                        <option @if($p->kota=="Kota Pasuruan") selected="selected" @endif value="Kota Pasuruan" name="inputkota">Kota Pasuruan</option>
                        <option @if($p->kota=="Kota Probolinggo") selected="selected" @endif value="Kota Probolinggo" name="inputkota">Kota Probolinggo</option>
                        <option @if($p->kota=="Kota Surabaya") selected="selected" @endif value="Kota Surabaya" name="inputkota">Kota Surabaya</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Asal Pos LL</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $p->asalpos }}" class="form-control" name="inputasalpos" placeholder="Asal Pos LL">
                </div>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-info float-right">Save</button>
            <a href="{{url('/hapusposll')}}/{{$p->id}}">
                <button type="button" class="btn btn-danger float-right">Delete</button>
              </a>
              <a href="{{url('/masterposll')}}">
                <button type="button" class="btn btn-default float-right"> Back to List </button>
              </a>
        </div>
        <!-- /.card-footer -->
        </form>
        @endforeach
    </div>
</section>
    <!-- /.content -->
</div>
@endsection