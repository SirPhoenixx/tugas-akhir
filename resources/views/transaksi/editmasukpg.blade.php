@extends('admin.admin')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Trip</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dash">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/lapevaluasi">Laporan Evaluasi</a></li>
              <li class="breadcrumb-item active">Evaluasi</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="card card-info">
        <div class="card-header">
        <h3 class="card-title">Detail Trip</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form class="form-horizontal" action="/updatemasukpg" method="post">
        {{ csrf_field() }}
        <div class="card-body">
            <input type="hidden" name="inputid" value="{{ $trip->id }}">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Plat Truk Masuk LL</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $trip->plat_truk_ll }}" class="form-control" name="inputplata" disabled>
                </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Plat Truk Masuk PG</label>
              <div class="col-sm-10">
                  <input type="text" required="required" value="{{ $trip->plat_truk_pg }}" class="form-control" name="inputplatb">
              </div>
          </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">No Kontrak Masuk LL</label>
                <div class="col-sm-10">
                    <select name="inputkontraka" class="form-control" disabled>
                      <option value="" selected="selected" disabled>Pilih Kontrak</option>
                        @foreach ($kk as $r)
                        <option 
                            @if( $trip->id_kontrak_ll == $r->id ) selected="selected" @endif 
                            value={{ $r->id }}>{{$r->nama}}
                        </option>    
                      @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">No Kontrak Masuk PG</label>
              <div class="col-sm-10">
                  <select name="inputkontrakb" class="form-control">
                    <option value="" selected="selected" disabled>Pilih Kontrak</option>
                      @foreach ($kk as $r)
                      <option 
                          @if( $trip->id_kontrak_pg == $r->id ) selected="selected" @endif 
                          value={{ $r->id }}>{{$r->nama}}
                      </option>    
                    @endforeach
                  </select>
              </div>
          </div>
            <input type="hidden" required="required" value="{{ $trip->tanggal_posll }}" class="form-control" name="inputtanggalposll">
            <input type="hidden" required="required" value="{{ $trip->tanggal_pg }}" class="form-control" name="inputtanggalpg">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Petugas LL</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $trip->petugas_ll }}" class="form-control" name="inputpetugasll" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Petugas PG</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $trip->petugas_pg }}" class="form-control" name="inputpetugaspg">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">SPTA</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $trip->spta }}" class="form-control" name="inputspta">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Keterangan</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $trip->keterangan }}" class="form-control" name="inputketerangan">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Kode Hitung</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $trip->kode_hitung }}" class="form-control" name="inputkode" disabled>
                </div>
            </div>
            <!-- /.checkbox -->
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">PG Tujuan</label>
                <div class="col-sm-10">
                <select name="inputpgtujuan" class="form-control" disabled>
                  @foreach ($pg as $s)
                  <option 
                        @if( $trip->id_pgtujuan == $s->id ) selected="selected" @endif 
                        value={{ $s->id }}>{{$s->nama}}
                    </option>   
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">PG Masuk</label>
              <div class="col-sm-10">
              <select name="inputpgmasuk" class="form-control">
                <option value="" selected="selected" disabled>Pilih PG</option>
                @foreach ($pg as $p)
                <option 
                      @if( $trip->id_pgmasuk == $p->id ) selected="selected" @endif 
                      value={{ $p->id }}>{{$p->nama}}
                  </option>   
                @endforeach
              </select>
            </div>
          </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Pos LL</label>
                <div class="col-sm-10">
                <select name="inputposll" class="form-control" disabled>
                  @foreach ($posll as $q)
                  <option 
                        @if( $trip->id_posll == $q->id ) selected="selected" @endif 
                        value={{ $q->id }}>{{$q->nama}}
                    </option>  
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Status</label>
                <div class="col-sm-10">
                    <select class="custom-select" name="inputstatus">
                        <option value="" selected="selected" disabled>Pilih Status</option>
                        <option @if( $trip->status == "Berhasil" ) selected="selected" @endif value="Berhasil" name="inputstatus">Berhasil</option>
                        <option @if( $trip->status == "Transfer - Berhasil" ) selected="selected" @endif value="Transfer - Berhasil" name="inputstatus">Transfer - Berhasil</option>
                        <option @if( $trip->status == "Ditolak - Berhasil" ) selected="selected" @endif value="Ditolak - Berhasil" name="inputstatus">Ditolak - Berhasil</option>
                        <option @if( $trip->status == "Ditolak" ) selected="selected" @endif value="Ditolak" name="inputstatus">Ditolak</option>
                        <option @if( $trip->status == "Ditolak dengan catatan" ) selected="selected" @endif value="Ditolak dengan catatan" name="inputstatus">Ditolak dengan catatan</option>
                    </select>
                </div>
            </div>

        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-info float-right">Save</button>
            <a href="{{url('/transaksimasukpg')}}">
              <button type="button" class="btn btn-default float-right"> Back to List </button>
            </a>
        </div>
        <!-- /.card-footer -->
        </form>
    </div>
</section>
    <!-- /.content -->
</div>
@endsection