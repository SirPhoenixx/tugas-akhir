@extends('admin.admin')
@section('content')
    
<div class="content-wrapper" style="min-height: 1200.88px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">  
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dash">Dashboard</a></li>
              <li class="breadcrumb-item active">Data Truk Masuk Pos LL</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content class="text-center" -->
    <section class="content">
      <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Trip Tebu Masuk Pos LL</h3>
                    <div class="actions">
                      <a href="/addmasukll" class="btn btn-default btn-xs float-right">Add  <i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="card-body">
                  <table class="table table-striped table-bordered table-list" id="tampil-masukll" style="width:100%">
                    <thead>
                        <tr role="row">
                            <th>PG</th>
                            <th>Pos LL</th>
                            <th>Kode Hitung</th>
                            <th>Plat Truk</th>
                            <th>No KK</th>
                            <th>Tgl masuk LL</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
                </div>
              </div>
              <!-- /.card -->
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('js')
<!-- Pastikan posisi dibawah import datatablesnya -->
<script type="text/javascript">
    $(function() {
        var oTable = $('#tampil-masukll').DataTable({
            processing: false,
            serverSide: true,
            order: [[ 5, "desc" ]],
            ajax: {
                url: '{{ url("tampilmasukll") }}'
            },
            columns: [
            {data: 'nama_pga',   name: 'nama_pga'},
            {data: 'nama_posll',     name: 'nama_posll'},
            {data: 'kode_hitung',   name: 'kode_hitung'},
            {data: 'plat_truk_ll', name: 'plat_truk_ll'},
            {data: 'nama_kka',  name: 'nama_kka'},
            {data: 'tanggal_posll', name: 'tanggal_posll'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            columnDefs:[{targets:5, render:function(data){
                return moment(data).format('LLL');
            }}]
        });
    });
</script>
@endsection
  
  