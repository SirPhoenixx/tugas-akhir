@extends('admin.admin')
@section('content')
    
<div class="content-wrapper" style="min-height: 1200.88px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">  
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dash">Dashboard</a></li>
              <li class="breadcrumb-item active">Data Truk Masuk PG</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content class="text-center" -->
    <section class="content">
      <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Trip Tebu Masuk PG</h3>
                </div>
                <div class="card-body">
                  <table class="table table-striped table-bordered table-list" id="tampil-masukpg" style="width:100%">
                    <thead>
                        <tr role="row">
                            <th>PG</th>
                            <th>Pos LL</th>
                            <th>Kode Hitung</th>
                            <th>Plat Truk</th>
                            <th>No KK</th>
                            <th>SPTA</th>
                            <th>Tgl masuk LL</th>
                            <th>Tgl masuk PG</th>
                            <th>Keterangan</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
                </div>
              </div>
              <!-- /.card -->
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('js')
<!-- Pastikan posisi dibawah import datatablesnya -->
<script type="text/javascript">
    $(function() {
        var oTable = $('#tampil-masukpg').DataTable({
            processing: false,
            serverSide: true,
            // order: [[ 7, "desc" ]],
            ajax: {
                url: '{{ url("tampilmasukpg") }}'
            },
            columns: [
            {data: 'nama_pgb',   name: 'nama_pgb'},
            {data: 'nama_posll',     name: 'nama_posll'},
            {data: 'kode_hitung',   name: 'kode'},
            {data: 'plat_truk_pg', name: 'plat'},
            {data: 'nama_kkb',  name: 'nama_kka'},
            {data: 'spta', name: 'spta'},
            {data: 'tanggal_posll', name: 'tanggal_posll'},
            {data: 'tanggal_pg', name: 'tanggal_pg'},
            {data: 'keterangan', name: 'keterangan'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            columnDefs:[{targets:[6, 7], render:function(data){
                return moment(data).format('LLL');
            }}]
        });
    });
</script>
@endsection
  
  