@extends('admin.admin')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Group</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dash">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/manajemengroup">Master Group</a></li>
              <li class="breadcrumb-item active">Add Group</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="card card-info">
        <div class="card-header">
        <h3 class="card-title">Detail Group</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form class="form-horizontal" action="/addedgroup" method="post">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nama Group</label>
                <div class="col-sm-10">
                    <input type="text" required="required" class="form-control" name="inputnama">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Credentials</label>
                <div class="col-sm-10">
                    <input type="text" required="required" class="form-control" name="inputcredentials">
                </div>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-info float-right">Save</button>
            <a href="{{url('/manajemengroup')}}">
              <button type="button" class="btn btn-default float-right"> Back to List </button>
            </a>
        </div>
        <!-- /.card-footer -->
        </form>
    </div>
</section>
    <!-- /.content -->
</div>
@endsection