@extends('admin.admin')
@section('content')
    
<div class="content-wrapper" style="min-height: 1200.88px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">  
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dash">Dashboard</a></li>
              <li class="breadcrumb-item active">User</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content class="text-center" -->
    <section class="content">
      <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                    <h3 class="card-title">User</h3>
                </div>
                <div class="card-body">
                  <table class="table table-striped table-bordered table-list" id="tampil-user" style="width:100%">
                    <thead>
                        <tr role="row">
                            <th>Username</th>
                            <th>Nama</th>
                            <th>Group</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
                </div>
              </div>
              <!-- /.card -->
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('js')
<!-- Pastikan posisi dibawah import datatablesnya -->
<script type="text/javascript">
    $(function() {
        var oTable = $('#tampil-user').DataTable({
            processing: false,
            serverSide: true,
            ajax: {
                url: '{{ url("tampiluser") }}'
            },
            columns: [
            {data: 'username',     name: 'username'},
            {data: 'nama',   name: 'nama'},
            {data: 'nama_group', name: 'nama_group'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        });
    });
</script>
@endsection
  
  