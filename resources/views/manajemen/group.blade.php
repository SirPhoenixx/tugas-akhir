@extends('admin.admin')
@section('content')
    
<div class="content-wrapper" style="min-height: 1200.88px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">  
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dash">Dashboard</a></li>
              <li class="breadcrumb-item active">Group User</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content class="text-center" -->
    <section class="content">
      <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Group User</h3>
                    <div class="actions">
                      <a href="/addgroup" class="btn btn-default btn-xs float-right">Add  <i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="card-body">
                  <table class="table table-striped table-bordered table-list" id="tampil-group" style="width:100%">
                    <thead>
                        <tr role="row">
                            <th>Nama Group </th>
                            <th>Create At</th>
                            <th>Update At</th>
                            <th>Credential</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
                </div>
              </div>
              <!-- /.card -->
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('js')
<!-- Pastikan posisi dibawah import datatablesnya -->
<script type="text/javascript">
    $(function() {
        var oTable = $('#tampil-group').DataTable({
            processing: false,
            serverSide: true,
            ajax: {
                url: '{{ url("tampilgroup") }}'
            },
            columns: [
            {data: 'nama',     name: 'nama'},
            {data: 'created_at',   name: 'created_at',  orderable: false},
            {data: 'updated_at', name: 'updated_at',  orderable: false},
            {data: 'credentials',  name: 'credentials',   orderable: false, searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            columnDefs:[{targets:[1, 2], render:function(data){
                return moment(data).format('LLL');
            }}]
        });
    });
</script>
@endsection
  
  