@extends('admin.admin')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dash">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/manajemenuser">User</a></li>
              <li class="breadcrumb-item active">Edit User</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="card card-info">
        <div class="card-header">
        <h3 class="card-title">Detail User</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form class="form-horizontal" action="/updateuser" method="post">
        {{ csrf_field() }}
        <div class="card-body">
            <input type="hidden" name="inputid" value="{{ $employee->id }}">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Username</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $employee->username }}" class="form-control" name="inputusername">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" required="required" value="{{ $employee->email }}" class="form-control" name="inputemail">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nama</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $employee->nama }}" class="form-control" name="inputnama">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nomer HP</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $employee->nomerhp }}" class="form-control" name="inputnomerhp">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Group</label>
                <div class="col-sm-10">
                <select name="inputgroup" class="form-control">
                  @foreach ($group as $r)
                  <option 
                  @if( $employee->id_group == $r->id ) selected="selected" @endif 
                  value={{ $r->id }}>{{$r->nama}}
                  </option>    
                  @endforeach
                </select>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-info float-right">Save</button>
            <a href="{{url('/hapususer')}}/{{$employee->id}}">
              <button type="button" class="btn btn-danger float-right">Delete</button>
            </a>
            <a href="{{url('/manajemenuser')}}">
              <button type="button" class="btn btn-default float-right"> Back to List </button>
            </a>
        </div>
        <!-- /.card-footer -->
        </form>
    </div>
</section>
    <!-- /.content -->
</div>
@endsection