@extends('admin.admin')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Group</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dash">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/manajemengroup">Manajemen Group</a></li>
              <li class="breadcrumb-item active">Edit Group</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="card card-info">
        <div class="card-header">
        <h3 class="card-title">Detail Group</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @foreach($group as $p)
        <form class="form-horizontal" action="/updategroup" method="post">
        {{ csrf_field() }}
        <div class="card-body">
            <input type="hidden" name="inputid" value="{{ $p->id }}">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nama Group</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $p->nama }}" class="form-control" name="inputnama" placeholder="Nama Group">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Credentials</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $p->credentials }}" class="form-control" name="inputcredentials" placeholder="Credenials">
                </div>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-info float-right">Save</button>
            <a href="{{url('/hapusgroup')}}/{{$p->id}}">
              <button type="button" class="btn btn-danger float-right">Delete</button>
            </a>
            <a href="{{url('/manajemengroup')}}">
              <button type="button" class="btn btn-default float-right"> Back to List </button>
            </a>
        </div>
        <!-- /.card-footer -->
        </form>
        @endforeach
    </div>
</section>
    <!-- /.content -->
</div>
@endsection