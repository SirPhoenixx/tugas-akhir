@extends('admin.admin')
@section('content')
    
<div class="content-wrapper" style="min-height: 1200.88px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">  
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dash">Dashboard</a></li>
              <li class="breadcrumb-item active">Data Truk Masuk Semua Pos</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content class="text-center" -->
    <section class="content">
      <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Trip Tebu Masuk Semua Pos</h3>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-md-6">
                      <h5>From Date <span class="text-danger"></span></h5>
                      <div class="controls">
                          <input type="date" name="from_date" id="from_date" class="form-control datepicker-autoclose" placeholder="Please from date">
                        </div>
                    </div>
      
                    <div class="form-group col-md-6">
                      <h5>To Date <span class="text-danger"></span></h5>
                      <div class="controls">
                          <input type="date" name="to_date" id="to_date" class="form-control datepicker-autoclose" placeholder="Please to date">
                      </div>
                    </div>
                    </div>
  
                    <div class="row">
                      <div class="form-group col-md-5">
                          <select name="inputpg" id="inputpg" class="form-control">
                            <option value="">Pilih PG Tujuan</option>
                            @foreach ($pg as $p)
                              <option value="{{$p->id}}">{{$p->nama}}</option>
                            @endforeach
                          </select>
                      </div>
        
                      <div class="form-group col-md-5">
                        <select name="inputposll" id="inputposll" class="form-control">
                          <option value="">Pilih Pos LL</option>
                          @foreach ($posll as $p)
                            <option value="{{$p->id}}">{{$p->nama}}</option>
                          @endforeach
                        </select>
                      </div>
  
                      <div class="form-group col-md-2" style="margin-bottom: 32px;">
                        <div class="controls">
                        <button type="text" id="btn-search" class="btn btn-info form-control">Submit</button>
                        </div>
                      </div>
                    </div>
                    
                  <table class="table table-striped table-bordered table-list" id="tampil-lapsemua" style="width:100%">
                    <thead>
                        <tr role="row">
                            <th>PG Tujuan</th>
                            <th>Pos LL</th>
                            <th>PG Masuk</th>
                            <th>Kode Hitung</th>
                            <th>Plat Truk</th>
                            <th>No KK</th>
                            <th>SPTA</th>
                            <th>Tgl masuk LL</th>
                            <th>Tgl masuk PG</th>
                        </tr>
                    </thead>
                </table>
                </div>
              </div>
              <!-- /.card -->
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('js')
<!-- Pastikan posisi dibawah import datatablesnya -->
<script type="text/javascript">
    $(document).ready(function(){
    $('#tampil-lapsemua').DataTable({
            processing: false,
            serverSide: true,
            dom: '<"html5buttons">Blfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            ajax: {
                url: '{{ url("lapsemua") }}',
                type: 'GET',
                data: function (d) {
                d.from_date = $('#from_date').val();
                d.to_date = $('#to_date').val();
                d.inputpg = $('#inputpg').val();
                d.inputposll = $('#inputposll').val();
                }
            },
            columns: [
            {data: 'nama_pga',      name: 'nama_pga'},
            {data: 'nama_posll',     name: 'nama_posll'},
            {data: 'nama_pgb',      name: 'nama_pgb'},
            {data: 'kode_hitung'},
            {data: 'plat_truk_pg', name: 'plat_truk'},
            {data: 'nama_kkb',  name: 'nama_kkb'},
            {data: 'spta', name: 'spta'},
            {data: 'tanggal_posll', name: 'tanggal_posll'},
            {data: 'tanggal_pg', name: 'tanggal_pg'}
            ],
            columnDefs:[{targets:[8, 7], render:function(data){
                return moment(data).format('LLL');
            }}]
        });
      });
      $('#btn-search').click(function(){
      $('#tampil-lapsemua').DataTable().draw(true);
    });
</script>
@endsection
  
  