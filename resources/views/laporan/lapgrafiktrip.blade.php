@extends('admin.admin')
@section('content')
    
<div class="content-wrapper" style="min-height: 1200.88px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">  
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dash">Dashboard</a></li>
              <li class="breadcrumb-item active">Grafik Trip</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content class="text-center" -->
    <section class="content">
      <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Trip Tebu Masuk PG</h3>
                </div>
                <form action="{{url('/lapgrafiktrip')}}" method="get" >
                  <div class="card-body">
                    <div class="row">
                    <div class="form-group col-md-5">
                      <h5>From Date <span class="text-danger"></span></h5>
                      <div class="controls">
                          <input type="date" name="from_date" id="from_date" class="form-control datepicker-autoclose" placeholder="Please from date">
                        </div>
                    </div>
      
                    <div class="form-group col-md-5">
                      <h5>To Date <span class="text-danger"></span></h5>
                      <div class="controls">
                          <input type="date" name="to_date" id="to_date" class="form-control datepicker-autoclose" placeholder="Please to date">
                      </div>
                    </div>
      
                    <div class="form-group col-md-2" style="margin-top: 32px;">
                      <div class="controls">
                      <button type="submit" id="btn-search" class="btn btn-info form-control">Submit</button>
                     </div>
                   </div>
                  </div>
                </form>
              </div>
              <!-- /.card -->
      </div>
    </section>
    <section class="content">
        <div class="card card-danger">
            <div class="card-header">
              <h3 class="card-title">Chart</h3>
            </div>
            <div class="col-md-12">
              <div class="card-body">
                <div id="chartMonth"></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="card-body">
                  <div id="chartPg"></div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="card-body">
                  <div id="chartPosll"></div>
                </div>
              </div>
            </div>
        </div>
    </section>

    <!-- /.content -->
  </div>
@endsection

@section('js')
<!-- Pastikan posisi dibawah import datatablesnya -->
<script type="text/javascript">
      $('#btn-search').click(function(){
      $('#tampil-lapmasukpg').DataTable().draw(true);
    });
</script>
@endsection
  
@section('chartdash')
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script>
    Highcharts.chart('chartMonth', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Trip berdasarkan bulan'
    },
    xAxis: {
        categories: {!!json_encode($month)!!},
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah Trip'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Trip',
        data: {!!json_encode($month_result)!!}

    }]
});
  </script>
  <script>
    Highcharts.chart('chartPg', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Data Trip per PG'
    },
    xAxis: {
        categories: {!!json_encode($categoriespg)!!},
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah Trip'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Trip',
        data: {!!json_encode($datapg)!!}

    }]
});
  </script>
  <script>
    Highcharts.chart('chartPosll', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Data Trip per Pos LL'
    },
    xAxis: {
        categories: {!!json_encode($categoriesll)!!},
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah Trip'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Trip',
        data: {!!json_encode($dataposll)!!}

    }]
});
  </script>
  @endsection
  