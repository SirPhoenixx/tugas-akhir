@extends('admin.admin')
@section('content')
    
<div class="content-wrapper" style="min-height: 1200.88px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">  
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dash">Dashboard</a></li>
              <li class="breadcrumb-item active">Data Kontrak</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content class="text-center" -->
    <section class="content">
      <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Kontrak</h3>
                </div>
                <div class="card-body">
                  <div class="row">
                </div>
                <table id="table3" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No Kontrak</th>
                      <th>Nama KK</th>
                      <th>hari ini</th>
                      <th>sd hari ini</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($categorieskk as $p)
                    <tr>
                      <td>{{$p}}</td>
                      <td>{{$nama[$p]}}</td>
                      <td>{{$todaykk[$p]}}</td>
                      <td>{{$sdtodaykk[$p]}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                </div>
              </div>
              <!-- /.card -->
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection
  
@section('js')
<script>
  $(document).ready(function(){
    $('#table3').DataTable({
        dom: '<"html5buttons">Blfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
  })
</script>
@endsection