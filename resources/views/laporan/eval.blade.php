@extends('admin.admin')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Evaluasi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dash">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/lapevaluasi">Laporan Evaluasi</a></li>
              <li class="breadcrumb-item active">Evaluasi</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="card card-info">
        <div class="card-header">
        <h3 class="card-title">Detail Transaksi</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form class="form-horizontal" action="/updateeval" method="post">
        {{ csrf_field() }}
        <div class="card-body">
            <input type="hidden" name="inputid" value="{{ $trip->id }}">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">ID Trip</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $trip->id }}" class="form-control" name="inputid" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">PG Penerima</label>
                <div class="col-sm-10">
                <select name="inputpgmasuk" class="form-control" disabled>
                    <option value="" selected="selected" >Pilih PG</option>
                    @foreach ($pg as $p)
                        <option 
                            @if( $trip->id_pgmasuk == $p->id ) selected="selected" @endif value={{ $p->id }}>{{$p->nama}}
                        </option>   
                    @endforeach
                </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">SPTA</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $trip->spta }}" class="form-control" name="inputid" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nopol LL</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $trip->plat_truk_ll }}" class="form-control" name="inputnopolll" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nopol PG</label>
                <div class="col-sm-10">
                    <input type="text" required="required" value="{{ $trip->plat_truk_pg }}" class="form-control" name="inputnopolpg" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Eval Nopol</label>
                <div class="col-sm-10">
                    <select class="custom-select" name="inputevalnopol">
                        <option value="" selected="selected" disabled>Pilih Eval</option>
                        <option @if($trip->eval_plat_truk=="Sesuai") selected="selected" @endif value="Sesuai" name="inputevalnopol">Sesuai</option>
                        <option @if($trip->eval_plat_truk=="Tidak Sesuai") selected="selected" @endif value="Tidak Sesuai" name="inputevalnopol">Tidak Sesuai</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">No Kontrak LL</label>
                <div class="col-sm-10">
                    <select name="inputkontrak" class="form-control" disabled>
                      <option value="" selected="selected">Pilih Kontrak</option>
                        @foreach ($kk as $r)
                        <option 
                            @if( $trip->id_kontrak_ll == $r->id ) selected="selected" @endif 
                            value={{ $r->id }}>{{$r->nama}}
                        </option>    
                      @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">No Kontrak PG</label>
                <div class="col-sm-10">
                    <select name="inputkontrak" class="form-control" disabled>
                      <option value="" selected="selected">Pilih Kontrak</option>
                        @foreach ($kk as $r)
                        <option 
                            @if( $trip->id_kontrak_ll == $r->id ) selected="selected" @endif 
                            value={{ $r->id }}>{{$r->nama}}
                        </option>    
                      @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Eval No Kontrak</label>
                <div class="col-sm-10">
                    <select class="custom-select" name="inputevalnokontrak">
                        <option value="" selected="selected" disabled>Pilih Eval</option>
                        <option @if($trip->eval_no_kontrak=="Sesuai") selected="selected" @endif value="Sesuai" name="inputevalnokontrak">Sesuai</option>
                        <option @if($trip->eval_no_kontrak=="Tidak Sesuai") selected="selected" @endif value="Tidak Sesuai" name="inputevalnokontrak">Tidak Sesuai</option>
                    </select>
                </div>
            </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Masuk LL</label>
            <div class="col-sm-10">
                <input type="text" required="required" value="{{ $trip->tanggal_posll }}" class="form-control" disabled>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Masuk PG</label>
            <div class="col-sm-10">
                <input type="text" required="required" value="{{ $trip->tanggal_pg }}" class="form-control" disabled>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Eval Waktu</label>
            <div class="col-sm-10">
                <input type="text" required="required" value="{{ $trip->eval_waktu }}" class="form-control" name="inputevalwaktu">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Hasil Eval</label>
            <div class="col-sm-10">
                <input type="text" required="required" value="{{ $trip->hasil_eval }}" class="form-control" name="inputhasileval">
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-info float-right">Save</button>
            <a href="{{url('/lapevaluasi')}}">
              <button type="button" class="btn btn-default float-right"> Back to List </button>
            </a>
        </div>
    </form>
        <!-- /.card-footer -->
    </div>
</section>
    <!-- /.content -->
</div>
@endsection