@extends('admin.admin')
@section('content')
    
<div class="content-wrapper" style="min-height: 1200.88px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">  
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dash">Dashboard</a></li>
              <li class="breadcrumb-item active">Laporan Evaluasi</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content class="text-center" -->
    <section class="content">
      <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Evaluasi Data</h3>
                </div>
                <div class="card-body">
                  <div class="row">
                  <div class="form-group col-md-5">
                    <h5>From Date <span class="text-danger"></span></h5>
                    <div class="controls">
                        <input type="date" name="from_date" id="from_date" class="form-control datepicker-autoclose" placeholder="Please from date">
                      </div>
                  </div>
    
                  <div class="form-group col-md-5">
                    <h5>To Date <span class="text-danger"></span></h5>
                    <div class="controls">
                        <input type="date" name="to_date" id="to_date" class="form-control datepicker-autoclose" placeholder="Please to date">
                    </div>
                  </div>
    
                  <div class="form-group col-md-2" style="margin-top: 32px;">
                    <div class="controls">
                    <button type="text" id="btn-search" class="btn btn-info form-control">Submit</button>
                   </div>
                 </div>
                </div>
                  <table class="table table-striped table-bordered table-list" id="tampil-lapevaluasi" style="width:100%">
                    <thead>
                      <tr>
                        <th rowspan="2">No</th>
                        <th rowspan="2">ID Trip</th>
                        <th rowspan="2">PG Penerima</th>
                        <th rowspan="2">SPTA</th>
                        <th colspan="3">Pos Legalisasi</th>
                        <th colspan="3">Pos Penerimaan PG</th>
                        <th colspan="3">Evaluasi</th>
                        <th rowspan="2">Status</th>
                        <th rowspan="2">Hasil Evaluasi</th>
                        <th rowspan="2">Tanggal Evaluasi</th>
                        <th rowspan="2">Actions</th>
                      </tr>
                      <tr>
                          <th>No Polisi</th>
                          <th>No Kontrak</th>
                          <th>Tanggal</th>
                          <th>No Polisi</th>
                          <th>No Kontrak</th>
                          <th>Tanggal</th>
                          <th>No Polisi</th>
                          <th>No Kontrak</th>
                          <th>Waktu Tempuh</th>
                      </tr>
                    </thead>
                </table>
                </div>
              </div>
              <!-- /.card -->
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('js')
<!-- Pastikan posisi dibawah import datatablesnya -->
<script type="text/javascript">
    $(document).ready(function(){
    $('#tampil-lapevaluasi').DataTable({
            processing: false,
            serverSide: true,
            "scrollX": true,
            dom: '<"html5buttons">Blfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            ajax: {
                url: '{{ url("lapevaluasi") }}',
                type: 'GET',
                data: function (d) {
                d.from_date = $('#from_date').val();
                d.to_date = $('#to_date').val();
                }
            },
            order: [[ 15, "desc" ]],
            columns: [
            {data: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'id',   name: 'id'},
            {data: 'nama_pgb',   name: 'nama_pgb'},
            {data: 'spta', name: 'spta'},
            {data: 'plat_truk_ll', name: 'plat_truk_ll'},
            {data: 'nama_kka',  name: 'nama_kka'},
            {data: 'tanggal_posll', name: 'tanggal_posll'},
            {data: 'plat_truk_pg', name: 'plat_truk_pg'},
            {data: 'nama_kkb',  name: 'nama_kkb'},
            {data: 'tanggal_pg', name: 'tanggal_pg'},
            {data: 'eval_plat_truk', name: 'eval_plat_truk'},
            {data: 'eval_no_kontrak',  name: 'eval_no_kontrak'},
            {data: 'eval_waktu',  name: 'eval_waktu'},
            {data: 'status',  name: 'status'},
            {data: 'hasil_eval',  name: 'hasil_eval'},
            {data: 'tanggal_eval', name: 'tanggal_eval'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            columnDefs:[{targets:[6, 9, 15], render:function(data){
                return moment(data).format('LLL');
            }}]
          });
      });
      $('#btn-search').click(function(){
      $('#tampil-lapevaluasi').DataTable().draw(true);
    });
</script>
@endsection
  
  