<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('petugas_pg')->nullable(true);
            $table->string('petugas_ll');
            $table->integer('spta')->nullable(true);
            $table->string('plat_truk_ll');
            $table->string('plat_truk_pg')->nullable(true);//pg
            $table->unsignedBigInteger('id_posll');
            $table->unsignedBigInteger('id_kontrak_ll');//nomerkontrak
            $table->unsignedBigInteger('id_kontrak_pg')->nullable(true);//nomerkontrak
            $table->unsignedBigInteger('id_pgtujuan');
            $table->unsignedBigInteger('id_pgmasuk')->nullable(true);
            $table->dateTime('tanggal_pg')->nullable(true);
            $table->dateTime('tanggal_posll');
            $table->integer('kode_hitung');
            $table->string('keterangan')->nullable(true);
            $table->string('status')->nullable(true);
            $table->string('eval_plat_truk')->nullable(true);
            $table->string('eval_no_kontrak')->nullable(true);
            $table->string('eval_waktu')->nullable(true);
            $table->string('hasil_eval')->nullable(true);
            $table->dateTime('tanggal_eval')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip');
    }
}
