<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTripTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trip', function (Blueprint $table) {
            $table->foreign('id_posll')->references('id')->on('posll')->onDelete('cascade');
            $table->foreign('id_kontrak_ll')->references('id')->on('kk')->onDelete('cascade');
            $table->foreign('id_kontrak_pg')->references('id')->on('kk')->onDelete('cascade');
            $table->foreign('id_pgtujuan')->references('id')->on('pg')->onDelete('cascade');
            $table->foreign('id_pgmasuk')->references('id')->on('pg')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
