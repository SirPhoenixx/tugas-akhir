<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePosllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posll', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kodepos');
            $table->string('nama');
            $table->string('alamat');
            $table->string('kota');
            $table->string('asalpos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posll');
    }
}
