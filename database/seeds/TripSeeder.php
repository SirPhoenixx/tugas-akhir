<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class TripSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        foreach(range(0,1000) as $i){
            DB::table('trip')->insert([
                'petugas_pg' => $faker->name,
                'petugas_ll' => $faker->name,
                'spta' => $faker->numberBetween($min = 10000, $max = 90000),
                'plat_truk_ll' => $faker->bothify('? #### ??'),
                'plat_truk_pg' => $faker->bothify('? #### ??'),
                'id_posll' => $faker->numberBetween($min = 1, $max = 33),
                'id_kontrak_ll' => $faker->numberBetween($min = 1, $max = 406),
                'id_kontrak_pg' => $faker->numberBetween($min = 1, $max = 406),
                'id_pgtujuan' => $faker->numberBetween($min = 1, $max = 11),
                'id_pgmasuk' => $faker->numberBetween($min = 1, $max = 11),
                'tanggal_pg' => $faker->dateTimeThisMonth($max = 'now', $timezone = null),
                'tanggal_posll' => $faker->dateTimeThisMonth($max = 'now', $timezone = null),
                'kode_hitung' => $faker->numberBetween($min = 1000, $max = 9999),
                'keterangan' => $faker->text,
                'status' => 'Berhasil',
                'eval_plat_truk' => 'Tidak Sesuai',
                'eval_no_kontrak' => 'Tidak Sesuai',
                'eval_waktu' => $faker->amPm($max = 'now'),
                'hasil_eval' => 'Tidak Sesuai',
                'tanggal_eval' => $faker->dateTimeThisMonth($max = 'now', $timezone = null)
            ]);
        }
    }
}
