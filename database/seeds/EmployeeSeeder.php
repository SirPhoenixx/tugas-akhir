<?php

use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// insert data ke table pegawai
        DB::table('employee')->insert([
            'nama' => 'Aqsal',
            'username' => 'root',
            'email' => 'aqsalarrijal@gmail.com',
            'nomerhp' => '089',
            'id_group' => 1,
            'foto' => 'ganteng',
            'password' => 'ngastemi',
            'id_pg' => 1
        ]);
    }
}
