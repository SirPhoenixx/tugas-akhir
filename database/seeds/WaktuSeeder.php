<?php

use Illuminate\Database\Seeder;

class WaktuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('waktu')->insert([
            'id_pg' => '10',
            'id_posll' => '11',
            'nilai' => '5'
        ]);
        DB::table('waktu')->insert([
            'id_pg' => '10',
            'id_posll' => '17',
            'nilai' => '4'
        ]);
        DB::table('waktu')->insert([
            'id_pg' => '10',
            'id_posll' => '33',
            'nilai' => '5'
        ]);
        DB::table('waktu')->insert([
            'id_pg' => '2',
            'id_posll' => '32',
            'nilai' => '5'
        ]);
        DB::table('waktu')->insert([
            'id_pg' => '3',
            'id_posll' => '32',
            'nilai' => '4'
        ]);
        DB::table('waktu')->insert([
            'id_pg' => '1',
            'id_posll' => '32',
            'nilai' => '3'
        ]);
        DB::table('waktu')->insert([
            'id_pg' => '1',
            'id_posll' => '30',
            'nilai' => '6'
        ]);
        DB::table('waktu')->insert([
            'id_pg' => '4',
            'id_posll' => '32',
            'nilai' => '2'//blum bisa dikoma
        ]);
    }
}
