<?php

use Illuminate\Database\Seeder;

class PosllSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('posll')->insert([
            'kodepos' => '060',
            'nama' => 'Kendal Payak',
            'alamat' => 'Malang',
            'kota' => 'Malang',
            'asalpos' => 'Malang Selatan - Turen'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '070',
            'nama' => 'Klakah / Leces',
            'alamat' => 'Lumajang',
            'kota' => 'Lumajang',
            'asalpos' => 'Lumajang'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '050',
            'nama' => 'Kencong',
            'alamat' => 'Jember',
            'kota' => 'Jember',
            'asalpos' => 'Jember'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '150',
            'nama' => 'Tanggul',
            'alamat' => 'Jember',
            'kota' => 'Jember',
            'asalpos' => 'Jember'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '080',
            'nama' => 'Kraton - Pasuruan',
            'alamat' => 'Pasuruan',
            'kota' => 'Pasuruan',
            'asalpos' => 'Probolinggo - Kota'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '080',
            'nama' => 'Kraton - Pasuruan',
            'alamat' => 'Pasuruan',
            'kota' => 'Pasuruan',
            'asalpos' => 'Pasuruan - Patalgrati'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '020',
            'nama' => 'Besuki / Paiton',
            'alamat' => 'Situbondo',
            'kota' => 'Situbondo',
            'asalpos' => 'Situbondo - Asembagus'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '020',
            'nama' => 'Besuki / Paiton',
            'alamat' => 'Situbondo',
            'kota' => 'Situbondo',
            'asalpos' => 'Bondowoso'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '020',
            'nama' => 'Besuki / Paiton',
            'alamat' => 'Situbondo',
            'kota' => 'Situbondo',
            'asalpos' => 'Banyuwangi'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '120',
            'nama' => 'Sale',
            'alamat' => 'Rembang',
            'kota' => 'Rembang',
            'asalpos' => 'Rembang, Pati & Kudus'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '010',
            'nama' => 'Banaran',
            'alamat' => 'Sragen',
            'kota' => 'Sragen',
            'asalpos' => 'Sragen'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '030',
            'nama' => 'Caruban',
            'alamat' => 'Madiun',
            'kota' => 'Madiun',
            'asalpos' => 'Magetan'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '030',
            'nama' => 'Caruban',
            'alamat' => 'Madiun',
            'kota' => 'Madiun',
            'asalpos' => 'Madiun'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '090',
            'nama' => 'Maospati',
            'alamat' => 'Madiun',
            'kota' => 'Madiun',
            'asalpos' => 'Ngawi'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '100',
            'nama' => 'Padangan-Cepu',
            'alamat' => 'Blora',
            'kota' => 'Blora',
            'asalpos' => 'Blora'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '130',
            'nama' => 'Sawoo',
            'alamat' => 'Ponorogo',
            'kota' => 'Ponorogo',
            'asalpos' => 'Ponorogo'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '110',
            'nama' => 'Reimuna',
            'alamat' => 'Malang',
            'kota' => 'Malang',
            'asalpos' => 'Malang Selatan dan Barat'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '040',
            'nama' => 'Gondanglegi',
            'alamat' => 'Malang',
            'kota' => 'Malang',
            'asalpos' => 'Malang Selatan dan Timur'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '140',
            'nama' => 'Singosari',
            'alamat' => 'Malang',
            'kota' => 'Malang',
            'asalpos' => 'Malang Utara - Singosari'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '205',
            'nama' => 'Bajulmati - Banyuwangi Timur	',
            'alamat' => 'Banyuwangi',
            'kota' => 'Banyuwangi',
            'asalpos' => 'Banyuwangi Timur'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '206',
            'nama' => 'Kalibaru Manis - Banyuwangi Selatan',
            'alamat' => 'Banyuwangi',
            'kota' => 'Banyuwangi',
            'asalpos' => 'Banyuwangi Selatan'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '207',
            'nama' => 'Kencong - Jember Selatan',
            'alamat' => 'Jember',
            'kota' => 'Jember',
            'asalpos' => 'Jember Selatan'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '208',
            'nama' => 'Besuki/Paiton - Situbondo/Bondowoso',
            'alamat' => 'Situbondo/Bondowoso',
            'kota' => 'Situbondo/Bondowoso',
            'asalpos' => 'Situbondo/Bondowoso'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '209',
            'nama' => 'Sumberbaru - Jember Utara',
            'alamat' => 'Jember',
            'kota' => 'Jember',
            'asalpos' => 'Jember Utara'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '210',
            'nama' => 'Leces/Ranuyoso - Lumajang',
            'alamat' => 'Lumajang',
            'kota' => 'Lumajang',
            'asalpos' => 'Lumajang'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '216',
            'nama' => 'Banaran - Sragen',
            'alamat' => 'Sragen',
            'kota' => 'Sragen',
            'asalpos' => 'Sragen'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '217',
            'nama' => 'Caruban - Magetan',
            'alamat' => 'Magetan',
            'kota' => 'Magetan',
            'asalpos' => 'Magetan'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '218',
            'nama' => 'Caruban - Madiun',
            'alamat' => 'Madiun',
            'kota' => 'Madiun',
            'asalpos' => 'Madiun'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '219',
            'nama' => 'Caruban - Ngawi',
            'alamat' => 'Ngawi',
            'kota' => 'Ngawi',
            'asalpos' => 'Ngawi'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '220',
            'nama' => 'Turen - Malang Selatan & Timur',
            'alamat' => 'Malang',
            'kota' => 'Malang',
            'asalpos' => 'Malang Selatan & Timur'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '221',
            'nama' => 'Gondanglegi - Malang Selatan & Timur',
            'alamat' => 'Malang',
            'kota' => 'Malang',
            'asalpos' => 'Malang Selatan & Timur'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '222',
            'nama' => 'Kalipare/Raimuna - Malang Selatan & Barat',
            'alamat' => 'Malang',
            'kota' => 'Malang',
            'asalpos' => 'Malang Selatan & Barat'
        ]);
        DB::table('posll')->insert([
            'kodepos' => '160',
            'nama' => 'Kalipare',
            'alamat' => 'Jember',
            'kota' => 'Jember',
            'asalpos' => 'Banyuwangi'
        ]);
    }
}
