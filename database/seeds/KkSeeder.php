<?php

use Illuminate\Database\Seeder;

class KkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('kk')->insert([
            'nokontrak' => '2685',
            'nama' => 'GANDI WIDODO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '9019',
            'nama' => 'YUSUF WIBOWO',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2684',
            'nama' => 'SAMSURI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '9018',
            'nama' => 'DEVI NOVITASARI',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '9017',
            'nama' => '	MISTAR',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2683',
            'nama' => 'DENI LESTARI WIDODO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2682',
            'nama' => 'SUDARTO, DRS',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2681',
            'nama' => 'HARMAJI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2679',
            'nama' => 'SAMSURI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2660',
            'nama' => 'NINIK ISWATI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2678',
            'nama' => 'UDIANTORO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2677',
            'nama' => 'SRI RAHAYU',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2676',
            'nama' => 'KASTAMUN',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2675',
            'nama' => 'H. Trisno',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2674',
            'nama' => 'MATAMIN',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2673',
            'nama' => 'SAMSURI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2668',
            'nama' => 'MUALI FATAH',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2667',
            'nama' => 'SUPRIYONO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2666',
            'nama' => 'UDIANTORO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2665',
            'nama' => 'SUJAIS',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2643',
            'nama' => 'ANWAR ROLY',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2644',
            'nama' => '	SUKARYATI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2645',
            'nama' => '	HARMAJI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2646',
            'nama' => 'JEMARI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2647',
            'nama' => 'DENI PURWANTO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2648',
            'nama' => 'SULAIMAN AGUNG NUR ILHAM',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2649',
            'nama' => 'MOH. SUYONO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2652',
            'nama' => 'HASAN BADRI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2656',
            'nama' => 'SADIN ABDULLAH',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2657',
            'nama' => 'ABDUL HAMID',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2642',
            'nama' => 'MUSTAIN MAKKI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2641',
            'nama' => '	AWAL HERI PUTRANTO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2639',
            'nama' => '	TUNIK',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2637',
            'nama' => 'ACHMAD SONHAJI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2636',
            'nama' => '	ABDUL HAMID',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2635',
            'nama' => 'KHOLIP',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2633',
            'nama' => 'HARYONO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2632',
            'nama' => 'JEMARI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2250',
            'nama' => 'SADIN ABDULLAH',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2631',
            'nama' => '	ARI RATNAWATI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2630',
            'nama' => 'SUJARWO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2629',
            'nama' => 'PARNI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2628',
            'nama' => '	SUKADI WIBISONO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2627',
            'nama' => 'ROMLI HENDRO M',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2626',
            'nama' => 'H. SUNARDI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2625',
            'nama' => 'SUDARNO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2624',
            'nama' => 'SUGIMAN',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2623',
            'nama' => 'SRI MURYANTI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2622',
            'nama' => 'GIYANTO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2621',
            'nama' => 'H. NANANG SUPRIYANTO, ST',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2620',
            'nama' => '	BAMBANG SARJONO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2619',
            'nama' => 'H SUPARTO JUBER',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2615',
            'nama' => 'HARIONO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2611',
            'nama' => 'YADIANTO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2609',
            'nama' => 'MARSUDI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2607',
            'nama' => 'MISNADI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2606',
            'nama' => 'SETIONO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2605',
            'nama' => 'RIADI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2604',
            'nama' => 'HERMAWAN',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2603',
            'nama' => 'SAMSURI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2601',
            'nama' => 'MARNOTO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2176',
            'nama' => 'DEVI KURNIAWATI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2179',
            'nama' => 'KASTAMUN',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2181',
            'nama' => 'TUTIK HARIANTI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2184',
            'nama' => 'SRI RAHAYU',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2186',
            'nama' => 'SETIONO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2188',
            'nama' => 'SRI RAHAYU',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2190',
            'nama' => 'DEVI KURNIAWATI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2237',
            'nama' => 'HARYONO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2238',
            'nama' => 'SLAMET TRI WIBOWO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2246',
            'nama' => 'SUPANDRI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2248',
            'nama' => 'TITIS SRI WAHYUNI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2249',
            'nama' => 'ATOK FASTIONO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2485',
            'nama' => 'DEVI KURNIAWATI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2486',
            'nama' => 'KHOLIP',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2487',
            'nama' => 'GANDI WIDODO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2488',
            'nama' => 'SUKARNO IR',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2489',
            'nama' => 'SRIANI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2490',
            'nama' => 'SUKARNO IR',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2491',
            'nama' => 'Sudarto, Drs',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2501',
            'nama' => 'H. Trisno',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2511',
            'nama' => 'H. YUSUF',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2512',
            'nama' => 'SUPRIONO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2513',
            'nama' => 'SUDIAN AGUS MARDIANTO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2520',
            'nama' => 'IBNU FAJAR',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2529',
            'nama' => 'REZA DWI FERNANDA',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2530',
            'nama' => 'SULAIMAN AGUNG NUR ILHAM',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2532',
            'nama' => 'SAMSUL ARIFIN',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2596',
            'nama' => 'ACHMAD SYAHID',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2599',
            'nama' => 'MUCH ICHWANTO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2600',
            'nama' => 'MUJIYANTO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2158',
            'nama' => 'PURYADI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2159',
            'nama' => 'SLIMURYANTO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2160',
            'nama' => 'SUGIONO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2161',
            'nama' => 'SULISTIO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2162',
            'nama' => 'KHOLIP',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2165',
            'nama' => 'MATNAHAR EKO BASUKI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2166',
            'nama' => 'MESONO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2167',
            'nama' => 'MESONO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2173',
            'nama' => 'DEVI KURNIAWATI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2175',
            'nama' => 'DEVI KURNIAWATI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0972',
            'nama' => 'JEMARI',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0973',
            'nama' => 'KASTAMUN',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1092',
            'nama' => 'MISNADI',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1201',
            'nama' => 'PRAYITNO',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '9015',
            'nama' => 'MISTAR',
            'id_pg' => '3',//ngadiredjo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '9016',
            'nama' => 'TUKIRAN KECEH',
            'id_pg' => '3',//ngadiredjo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1262',
            'nama' => 'DWI KURNIAWATI',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1263',
            'nama' => 'SINTO WATI LATRI',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2154',
            'nama' => 'NAWANG DWIANA',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2156',
            'nama' => 'JONY MUJIANTO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1180',
            'nama' => 'SETIONO',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0982',
            'nama' => 'SLIMURYANTO',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0979',
            'nama' => 'Sudarto, Drs',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1187',
            'nama' => 'SUPANDRI',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0981',
            'nama' => 'SUPANDRI',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1184',
            'nama' => 'SUPRIYONO',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1185',
            'nama' => 'UDIANTORO',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1186',
            'nama' => 'ZAENAL ARIFIN',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0969',
            'nama' => 'HERU TRIYONO',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1198',
            'nama' => 'IBNU FAJAR',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0742',
            'nama' => 'SUPARNGADI',
            'id_pg' => '7',//djombang baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0741',
            'nama' => 'MAMIK YULIANA',
            'id_pg' => '7',//djombang baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0898',
            'nama' => 'MITA PARDI NARTIKA',
            'id_pg' => '7',//djombang baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0746',
            'nama' => 'YUWONO',
            'id_pg' => '7',//djombang baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0687',
            'nama' => 'ACHMAD SYAHID',
            'id_pg' => '7',//djombang baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0777',
            'nama' => 'SAFII IHSAN',
            'id_pg' => '7',//djombang baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0740',
            'nama' => 'MOHAMAT ASOK',
            'id_pg' => '7',//djombang baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0686',
            'nama' => 'SUPARNGADI',
            'id_pg' => '7',//djombang baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0897',
            'nama' => 'PURYADI',
            'id_pg' => '7',//djombang baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0688',
            'nama' => 'AKHMAD TRI SEPTYAN',
            'id_pg' => '7',//djombang baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0896',
            'nama' => 'ERIK DARWANTO',
            'id_pg' => '7',//djombang baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0743',
            'nama' => 'SAFII IHSAN',
            'id_pg' => '7',//djombang baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1171',
            'nama' => 'ROSIDI',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1166',
            'nama' => 'WISNU SAPUTRA',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1476',
            'nama' => 'SAIFUL ANWAR',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1529',
            'nama' => 'RIDA LUPITA CANDRA',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1516',
            'nama' => 'DENDI SANJAYA',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1173',
            'nama' => 'GANDI WIDODO',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1642',
            'nama' => 'ENTIN IMA MIRYANTI',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1650',
            'nama' => 'RUDI HARTONO',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1524',
            'nama' => 'SAMSURI',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1672',
            'nama' => 'RUSIANA',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1643',
            'nama' => 'LILIK ANA CAHYAWATI',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1522',
            'nama' => 'SUPRIYONO',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1509',
            'nama' => 'NURYATIM ANWAR HUDA',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1481',
            'nama' => 'SISWADI EKO',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1169',
            'nama' => 'HERMANTO',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1159',
            'nama' => 'SLIMURYANTO',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1511',
            'nama' => 'ABDUL HAKIM',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1525',
            'nama' => 'ROHMAT HASAN',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1647',
            'nama' => 'SUJARI',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1167',
            'nama' => 'SRI UTAMI',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1161',
            'nama' => 'SUPANDRI',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1168',
            'nama' => 'NASUHA',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1479',
            'nama' => 'YANTO',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1172',
            'nama' => 'NAWANG DWIANA',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1477',
            'nama' => 'ALUNG ANGGARIA',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1507',
            'nama' => 'RADIANTO',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1510',
            'nama' => 'MATAMIN',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1515',
            'nama' => 'AGUS WIJAYADI E. P.',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1523',
            'nama' => 'MAHRUM ROHMAD',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1528',
            'nama' => 'JAMIATI NURUL CHOTIMAH',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1535',
            'nama' => 'ABDOR ROCHIM',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1646',
            'nama' => 'SURADI',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1659',
            'nama' => 'DIVANI LINDIA',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1154',
            'nama' => 'SETIONO',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1534',
            'nama' => 'ABDUL GOFUR',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1527',
            'nama' => 'ARIES BUDIARTO',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1526',
            'nama' => 'WASIS EKO SUSANTO',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1513',
            'nama' => 'BUDIONO',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1512',
            'nama' => 'NURHADI',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1508',
            'nama' => 'ROFIK MARJOKO',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1482',
            'nama' => 'SUTIKNO HASANUDIN',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1155',
            'nama' => 'OVICA INDRA IRAWAN',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1158',
            'nama' => 'IKA PURWANTI',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1162',
            'nama' => 'TUTIK HARIANTI',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1160',
            'nama' => 'PANUT',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1480',
            'nama' => 'UDIANTORO',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1644',
            'nama' => 'ARIN SALSA BELLA',
            'id_pg' => '9',//lestari
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5236',
            'nama' => 'ROCHIM',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5071',
            'nama' => 'KUSWANDI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5070',
            'nama' => 'SOLIKIN DIDIK ARIADI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5069',
            'nama' => 'MISTIANI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5068',
            'nama' => 'AGUS SUTAWAN',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5238',
            'nama' => 'ANTON PRAYOGO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5237',
            'nama' => 'LANNY LINGGAWATI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5065',
            'nama' => 'SITI KULSUM',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5235',
            'nama' => 'MAJURI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5064',
            'nama' => 'GUNAWAN PRANTIONO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5049',
            'nama' => 'BUDIONO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5234',
            'nama' => 'YANTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5232',
            'nama' => 'TUTIK HARIANTI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5063',
            'nama' => 'HENDYC WAHYUDI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5256',
            'nama' => 'SLAMET SANTOSO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5255',
            'nama' => 'UDIANTORO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5062',
            'nama' => 'MOKHAMAD TOYIB',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5048',
            'nama' => 'MARNOTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5038',
            'nama' => 'PRAYITNO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5254',
            'nama' => 'PANUT',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5041',
            'nama' => 'SAFII IHSAN',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5061',
            'nama' => 'AGUS ZAENAL',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5087',
            'nama' => 'YUSUF HADI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5086',
            'nama' => 'HERI NURIANTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5060',
            'nama' => 'SAEDI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5047',
            'nama' => 'MISNADI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5085',
            'nama' => 'TEGUH WALUYO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5084',
            'nama' => 'FILA ESTU EKO MULYO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5059',
            'nama' => 'KUSPIYANTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5083',
            'nama' => 'JEMANI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5082',
            'nama' => 'FAISAL NATA BUANA',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5058',
            'nama' => 'IPUNG KURNIAWAN',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5046',
            'nama' => 'SUROSO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5037',
            'nama' => 'PAMBUDI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5034',
            'nama' => 'SRI MURYANTI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5061',
            'nama' => 'AGUS ZAENAL',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5060',
            'nama' => 'SAEDI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5081',
            'nama' => 'WAGIRAN',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5062',
            'nama' => 'MOKHAMAD TOIYIB',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5063',
            'nama' => 'HENDIC WAHYUDI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5064',
            'nama' => 'GUNAWAN PRANTIONO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5065',
            'nama' => 'SITI KULSUM',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5059',
            'nama' => 'KUSPIYANTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5058',
            'nama' => 'IPUNG KURNIAWAN',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0417',
            'nama' => 'IBNU FAJAR',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '	0973',
            'nama' => 'GUNAWAN PRANTIONO',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0969',
            'nama' => 'MU`ALI FATAH',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0967',
            'nama' => 'FAIZAL NUR ALYA BIMASYAH',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0964',
            'nama' => 'H. SUPARTO JUBER',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0963',
            'nama' => 'SRI MURYANTI',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0962',
            'nama' => 'H. NANANG SUPRIYANTO, ST',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0961',
            'nama' => 'GIYANTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0966',
            'nama' => 'YUSUF WIBOWO',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0965',
            'nama' => 'BUDHI SUHARIANTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0960',
            'nama' => 'MATJULI',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0959',
            'nama' => 'YEMBRI KRISTIANTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0958',
            'nama' => 'YUSUF HADI',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0957',
            'nama' => 'YANTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0956',
            'nama' => 'TUTIK HARIANTI',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0955',
            'nama' => 'FILA ESTU EKO MULYO',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0805',
            'nama' => 'KUSNIYAH',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0801',
            'nama' => 'UDIANTORO',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0799',
            'nama' => 'PANUT',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0798',
            'nama' => 'MARNOTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0797',
            'nama' => 'JEMARI',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0477',
            'nama' => 'IR. BAMBANG SULISTYONO, MSC.',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0475',
            'nama' => 'TEGUH WALUYO',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0473',
            'nama' => 'JEMANI',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0472',
            'nama' => 'AGUS SUTAWAN',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0471',
            'nama' => 'GATOT SUPRAPTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0470',
            'nama' => 'SURADI',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0469',
            'nama' => 'SITI KULSUM',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0468',
            'nama' => 'HENDYC WAHYUDI',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0467',
            'nama' => 'BUDIONO',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0466',
            'nama' => 'SUROSO',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0465',
            'nama' => 'SUPRIYONO',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0464',
            'nama' => 'SAMSURI',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0454',
            'nama' => 'DEDY KURNIAWAN',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0452',
            'nama' => 'SLIMURYANTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0453',
            'nama' => 'LILIK ANA CAHYAWATI',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0450',
            'nama' => 'WAGIRAN',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0451',
            'nama' => 'HERI NURIANTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0434',
            'nama' => 'SOFA UNTUNG FADILAH',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0433',
            'nama' => 'SRI NURYANTI',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0432',
            'nama' => 'LASIRAN',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0430',
            'nama' => 'KUSPIYANTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0418',
            'nama' => 'ANTON PRAYOGO',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5045',
            'nama' => 'SUPRIYONO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0419',
            'nama' => 'SARLIN',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0422',
            'nama' => 'IPUNG KURNIAWAN',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5036',
            'nama' => 'MARSUDI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5044',
            'nama' => 'SUBIYANTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5054',
            'nama' => 'MARIYA',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5074',
            'nama' => 'PUPUT WIDIANTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5046',
            'nama' => 'SUROSO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5047',
            'nama' => 'MISNADI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5075',
            'nama' => 'SIATI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5048',
            'nama' => 'MARNOTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5049',
            'nama' => 'BUDIONO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5055',
            'nama' => 'SRI UTAMI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5076',
            'nama' => 'UMAR',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5050',
            'nama' => 'JEMARI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5051',
            'nama' => 'GAGAS PRASTYO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5077',
            'nama' => 'TUKI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5052',
            'nama' => 'DEDY KURNIAWAN',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5053',
            'nama' => 'LILIS WIRETNOWATI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5045',
            'nama' => 'SUPRIONO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5056',
            'nama' => 'YAMBRI KRISTIANTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5078',
            'nama' => 'SRI NURYANTI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5054',
            'nama' => 'MARIYA',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5055',
            'nama' => 'SRI UTAMI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5079',
            'nama' => 'SOFA UNTUNG FADILAH',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5056',
            'nama' => 'YEMBRI KRISTIANTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5057',
            'nama' => 'SARLIN',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5057',
            'nama' => 'SARLIN',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5080',
            'nama' => 'KUSNIYAH',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5237',
            'nama' => 'LANNY LINGGAWATI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5236',
            'nama' => 'ROCHIM',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5235',
            'nama' => 'MAJURI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5234',
            'nama' => 'YANTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5233',
            'nama' => 'IR. BAMBANG SULISTYONO, MSC',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5232',
            'nama' => 'TUTIK HARIANTI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5087',
            'nama' => 'YUSUF HADI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5086',
            'nama' => 'HERI NURIANTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5085',
            'nama' => 'TEGUH WALUYO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5084',
            'nama' => 'FILA ESTU EKO MULYO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5083',
            'nama' => 'JEMANI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5082',
            'nama' => 'FAISAL NATA BUANA',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5081',
            'nama' => 'WAGIRAN',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5080',
            'nama' => 'KUSNIYAH',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5079',
            'nama' => 'SOFA UNTUNG FADILAH',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5078',
            'nama' => 'SRI NURYANTI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5077',
            'nama' => 'TUKI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5076',
            'nama' => 'UMAR',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5075',
            'nama' => 'SIATI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5074',
            'nama' => 'PUPUT WIDIANTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5073',
            'nama' => 'MADAHLAN',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5072',
            'nama' => 'LASIRAN',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0981',
            'nama' => 'SUHARI',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0976',
            'nama' => 'AGUS ZAENAL',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0977',
            'nama' => 'LULUK WULANDARI',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0972',
            'nama' => 'MARSUDI',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0978',
            'nama' => 'MISPAN WIDODO',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0968',
            'nama' => 'NAWANG RATNANINGSIH',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0968',
            'nama' => 'NAWANG RATNANINGSIH',
            'id_pg' => '10',//meritjan
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5073',
            'nama' => 'MADAHLAN',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5072',
            'nama' => 'LASIRAN',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5071',
            'nama' => 'KUSWANDI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5070',
            'nama' => 'SOLIKIN DIDK ARIADI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5069',
            'nama' => 'MISTIANI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5068',
            'nama' => 'AGUS SUTAWAN',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5067',
            'nama' => 'GATOT SUPRAPTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5066',
            'nama' => 'SURADI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5053',
            'nama' => 'LILIS WIRETNOWATI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5052',
            'nama' => 'DEDY KURNIAWAN',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5051',
            'nama' => 'GAGAS PRASETYO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5050',
            'nama' => 'JEMARI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5040',
            'nama' => 'IBNU FAJAR',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5039',
            'nama' => 'SLIMURYANTO',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5035',
            'nama' => 'SAMSURI',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '5033',
            'nama' => 'H. NANANG SUPRIYANTO, ST',
            'id_pg' => '10',//meritjan
            'tahun' => '2018'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2638',
            'nama' => 'TUNIK',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2251',
            'nama' => 'SUKARYATI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2654',
            'nama' => 'ABDUL HAMID',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2614',
            'nama' => 'SUPRIONO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2608',
            'nama' => 'SUPRIYONO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2602',
            'nama' => 'HILAL MUSTAKIM',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2598',
            'nama' => 'M MARIDAN',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2597',
            'nama' => 'JEMARI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2595',
            'nama' => 'HARMAJI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2594',
            'nama' => 'UDIANTORO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2525',
            'nama' => 'PURYADI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2519',
            'nama' => 'NAWANG DWIANA',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2498',
            'nama' => 'SUDARTO, DRS.',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2497',
            'nama' => 'GALIH SATRIO SUSENO AJI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2492',
            'nama' => 'SUTRISNO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2256',
            'nama' => 'Cindy Windya Sari',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2236',
            'nama' => 'KHOLIP',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2235',
            'nama' => 'KHOLIP',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2234',
            'nama' => 'HENDIK KRISDYANTO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2233',
            'nama' => 'HARYONO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2187',
            'nama' => 'GANDI WIDODO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2185',
            'nama' => 'GANDI WIDODO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2174',
            'nama' => 'DEVI KURNIAWATI',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2157',
            'nama' => 'KHOLIP',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2149',
            'nama' => 'DENI LESTARI WIDODO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2146',
            'nama' => 'SLIMURYANTO',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2144',
            'nama' => 'NAWANG DWIANA',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '2653',
            'nama' => 'SADIN ABDULLAH',
            'id_pg' => '4',//pesantren baru
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1773',
            'nama' => 'SULISTIO',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1774',
            'nama' => 'NAWANG DWIANA',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '9014',
            'nama' => 'OVICA INDRA IRAWAN',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '9013',
            'nama' => 'SAMUJI',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '9011',
            'nama' => 'KASMINI',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1775',
            'nama' => 'INDIKA WIRATMOKO',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1772',
            'nama' => 'SUKARYAMANTO',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1769',
            'nama' => 'WARI',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1765',
            'nama' => 'MESONO',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1763',
            'nama' => 'MUHAMMAD A G',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1762',
            'nama' => 'WISNU SAPUTRA',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1756',
            'nama' => 'PUPUT WIDIANTO',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1753',
            'nama' => 'AWAL HERI PUTRANTO',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1752',
            'nama' => 'SETIONO',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1750',
            'nama' => 'YUWONO',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1749',
            'nama' => 'SUROSO',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1748',
            'nama' => 'SLIMURYANTO',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1747',
            'nama' => 'ALUNG ANGGARIA',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1745',
            'nama' => 'DIAH AYU RATNA DELLA',
            'id_pg' => '3',//ngadirejo
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1209',
            'nama' => 'TUTIK HARIANTI',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1194',
            'nama' => 'WISNU SAPUTRA',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1210',
            'nama' => 'ABDUL GOFUR',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1183',
            'nama' => 'HANIFATUL HASANAH',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1181',
            'nama' => 'SAIFUL ANWAR',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1189',
            'nama' => 'ROSIDI',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0974',
            'nama' => 'NURDIANTO',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1101',
            'nama' => 'MUALI FATAH',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0978',
            'nama' => 'MOHAMAT ASOK',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1229',
            'nama' => 'MISPAN WIDODO',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1192',
            'nama' => 'MESIADI',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1193',
            'nama' => 'MATAMIN',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '0968',
            'nama' => 'MARSUDI',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1103',
            'nama' => 'JAMIATI NURUL CHOTIMAH',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1098',
            'nama' => 'HERMANTO',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1191',
            'nama' => 'PANUT',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1094',
            'nama' => 'ALUNG ANGGARIA',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
        DB::table('kk')->insert([
            'nokontrak' => '1178',
            'nama' => 'HERMANTO',
            'id_pg' => '1',//modjopanggong
            'tahun' => '2019'
        ]);
    }
}
