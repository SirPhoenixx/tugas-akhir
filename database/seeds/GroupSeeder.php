<?php

use Illuminate\Database\Seeder;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('group')->insert([
            'nama' => 'Developer',
            'credentials' => 'Developer Narada'
        ]);
        DB::table('group')->insert([
            'nama' => 'Admin PG',
            'credentials' => 'Admin tiap pabrik gula'
        ]);
        DB::table('group')->insert([
            'nama' => 'Admin Pos LL',
            'credentials' => 'Admin tiap pos ll'
        ]);
        DB::table('group')->insert([
            'nama' => 'Admin Pusat',
            'credentials' => 'Admin dari kantor pusat'
        ]);
        DB::table('group')->insert([
            'nama' => 'Evaluator',
            'credentials' => 'Bagian evaluasi'
        ]);
    }
}
