<?php

use Illuminate\Database\Seeder;
use App\Pg;

class PgSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Pg::insert([
            'kodepg' => 'PG13',
            'nama' => 'PG MODJOPANGGOONG',
            'alamat' => 'Kec. Kauman',
            'emailpg' => 'pg.modjopanggoong@ptpn10.co.id',
            'kota' => 'Tulungagung',
            'unit' => 'Pabrik Gula'
        ]);
        Pg::insert([
            'kodepg' => 'PG09',
            'nama' => 'PG LESTARI',
            'alamat' => 'Kec. Patianrowo',
            'emailpg' => 'pg.lestari@ptpn10.co.id',
            'kota' => 'Nganjuk',
            'unit' => 'Pabrik Gula'
        ]);
        Pg::insert([
            'kodepg' => 'PG12',
            'nama' => 'PG NGADIREDJO',
            'alamat' => 'Kec. Kras',
            'emailpg' => 'pg.ngadiredjo@ptpn10.co.id',
            'kota' => 'Kediri',
            'unit' => 'Pabrik Gula'
        ]);
        Pg::insert([
            'kodepg' => 'PG11',
            'nama' => 'PG PESANTREN BARU',
            'alamat' => 'Kec. Pesantren',
            'emailpg' => 'pg.pesantrenbaru@ptpn10.co.id',
            'kota' => 'Mojokerto',
            'unit' => 'Pabrik Gula'
        ]);
        Pg::insert([
            'kodepg' => 'PG08',
            'nama' => 'PG TJOEKIR',
            'alamat' => 'Kec. Diwek',
            'emailpg' => 'pg.tjoekir@ptpn10.co.id',
            'kota' => 'Jombang',
            'unit' => 'Pabrik Gula'
        ]);
        Pg::insert([
            'kodepg' => 'PG04',
            'nama' => 'PG TOELANGAN',
            'alamat' => 'Kec. Tulangan',
            'emailpg' => 'pg.toelangan@ptpn10.co.id',
            'kota' => 'Sidoarjo',
            'unit' => 'Pabrik Gula'
        ]);
        Pg::insert([
            'kodepg' => 'PG07',
            'nama' => 'PG DJOMBANG BARU',
            'alamat' => 'Kec. Jombang',
            'emailpg' => 'pg.djombangbaru@ptpn10.co.id',
            'kota' => 'Jombang',
            'unit' => 'Pabrik Gula'
        ]);
        Pg::insert([
            'kodepg' => 'PG06',
            'nama' => 'PG GEMPOLKREP',
            'alamat' => 'Kec. Gedek',
            'emailpg' => 'pg.gempolkrep@ptpn10.co.id',
            'kota' => 'Mojokerto',
            'unit' => 'Pabrik Gula'
        ]);
        Pg::insert([
            'kodepg' => 'PG05',
            'nama' => 'PG KREMBOONG',
            'alamat' => 'Kec. Krembung',
            'emailpg' => 'pg.kremboong@ptpn10.co.id',
            'kota' => 'Sidoarjo',
            'unit' => 'Pabrik Gula'
        ]);
        Pg::insert([
            'kodepg' => 'PG10',
            'nama' => 'PG MERITJAN',
            'alamat' => 'Kec. Mojoroto',
            'emailpg' => 'pg.meritjan@ptpn10.co.id',
            'kota' => 'Kediri',
            'unit' => 'Pabrik Gula'
        ]);
        Pg::insert([
            'kodepg' => 'PG03',
            'nama' => 'PG WATOETOELIS',
            'alamat' => 'Kec. Prambon',
            'emailpg' => 'pg.watoetoelis@ptpn10.co.id',
            'kota' => 'Sidoarjo',
            'unit' => 'Pabrik Gula'
        ]);
        Pg::insert([
            'kodepg' => 'KP01',
            'nama' => 'KANTOR PUSAT',
            'alamat' => 'Jl. Jembatan Merah No 3-11',
            'emailpg' => 'ptpn10@gmail.com',
            'kota' => 'Surabaya',
            'unit' => 'Kantor Pusat'
        ]);
    }
}
