<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Route::get('/dash', 'LoginController@index');
Route::get('/login', 'LoginController@login');
Route::post('/validasi', 'LoginController@validasi');
Route::get('/logout', 'LoginController@logout');

Route::get('/admin', 'AdminController@index');
Route::get('/dash', 'DashboardController@dashboard')->name('master.dashboard')->middleware('loginNaradaAuth');

Route::get('/manajemenuser', 'UserController@employee')->name('manajemen.user')->middleware('loginNaradaAuth');
Route::get('/tampiluser', 'UserController@tampiluser')->middleware('loginNaradaAuth');
Route::get('/edituser/{id}','UserController@edit')->middleware('loginNaradaAuth');
Route::post('/updateuser','UserController@update')->middleware('loginNaradaAuth');
Route::get('/hapususer/{id}','UserController@hapus')->middleware('loginNaradaAuth');

Route::get('/manajemengroup', 'GroupController@group')->name('manajemen.group')->middleware('loginNaradaAuth');
Route::get('/tampilgroup', 'GroupController@tampilgroup')->middleware('loginNaradaAuth');
Route::get('/addgroup','GroupController@add')->middleware('loginNaradaAuth');
Route::post('/addedgroup','GroupController@added')->middleware('loginNaradaAuth');
Route::get('/editgroup/{id}','GroupController@edit')->middleware('loginNaradaAuth');
Route::post('/updategroup','GroupController@update')->middleware('loginNaradaAuth');
Route::get('/hapusgroup/{id}','GroupController@hapus')->middleware('loginNaradaAuth');

Route::get('/manajemenmaster', 'MasterController@employee')->name('manajemen.master')->middleware('loginNaradaAuth');
Route::get('/tampilemployee', 'MasterController@tampilemployee')->middleware('loginNaradaAuth');
Route::get('/addemployee','MasterController@add')->middleware('loginNaradaAuth');
Route::post('/addedemployee','MasterController@added')->middleware('loginNaradaAuth');
Route::get('/editemployee/{id}','MasterController@edit')->middleware('loginNaradaAuth');
Route::post('/updateemployee','MasterController@update')->middleware('loginNaradaAuth');
Route::get('/hapusemployee/{id}','MasterController@hapus')->middleware('loginNaradaAuth');

Route::get('/masterpg', 'PgController@pg')->name('master.pg')->middleware('loginNaradaAuth');
Route::get('/tampilpg', 'PgController@tampilpg')->middleware('loginNaradaAuth');
Route::get('/addpg','PgController@add')->middleware('loginNaradaAuth');
Route::post('/addedpg','PgController@added')->middleware('loginNaradaAuth');
Route::get('/editpg/{id}','PgController@edit')->middleware('loginNaradaAuth');
Route::post('/updatepg','PgController@update')->middleware('loginNaradaAuth');
Route::get('/hapuspg/{id}','PgController@hapus')->middleware('loginNaradaAuth');

Route::get('/masterposll', 'PosllController@posll')->name('master.posll')->middleware('loginNaradaAuth');
Route::get('/tampilposll', 'PosllController@tampilposll')->middleware('loginNaradaAuth');
Route::get('/addposll','PosllController@add')->middleware('loginNaradaAuth');
Route::post('/addedposll','PosllController@added')->middleware('loginNaradaAuth');
Route::get('/editposll/{id}','PosllController@edit')->middleware('loginNaradaAuth');
Route::post('/updateposll','PosllController@update')->middleware('loginNaradaAuth');
Route::get('/hapusposll/{id}','PosllController@hapus')->middleware('loginNaradaAuth');

Route::get('/masterkk', 'KkController@kk')->name('master.kk')->middleware('loginNaradaAuth');
Route::get('/tampilkk', 'KkController@tampilkk')->middleware('loginNaradaAuth');
Route::get('/addkk','KkController@add')->middleware('loginNaradaAuth');
Route::post('/addedkk','KkController@added')->middleware('loginNaradaAuth');
Route::get('/editkk/{id}','KkController@edit')->middleware('loginNaradaAuth');
Route::post('/updatekk','KkController@update')->middleware('loginNaradaAuth');
Route::get('/hapuskk/{id}','KkController@hapus')->middleware('loginNaradaAuth');

Route::get('/masterwaktu', 'WaktuController@waktu')->name('master.waktu')->middleware('loginNaradaAuth');
Route::get('/tampilwaktu', 'WaktuController@tampilwaktu')->middleware('loginNaradaAuth');
Route::get('/addwaktu','WaktuController@add')->middleware('loginNaradaAuth');
Route::post('/addedwaktu','WaktuController@added')->middleware('loginNaradaAuth');
Route::get('/editwaktu/{id}','WaktuController@edit')->middleware('loginNaradaAuth');
Route::post('/updatewaktu','WaktuController@update')->middleware('loginNaradaAuth');
Route::get('/hapuswaktu/{id}','WaktuController@hapus')->middleware('loginNaradaAuth');

Route::get('/transaksimasukpg', 'MasukpgController@masukpg')->name('transaksi.masukpg')->middleware('loginNaradaAuth');
Route::get('/tampilmasukpg', 'MasukpgController@tampilmasukpg')->middleware('loginNaradaAuth');
Route::get('/editmasukpg/{id}','MasukpgController@edit')->middleware('loginNaradaAuth');
Route::post('/updatemasukpg','MasukpgController@update')->middleware('loginNaradaAuth');
Route::get('/hapusmasukpg/{id}','MasukpgController@hapus')->middleware('loginNaradaAuth');

Route::get('/transaksimasukll', 'MasukllController@masukll')->name('transaksi.masukll')->middleware('loginNaradaAuth');
Route::get('/tampilmasukll', 'MasukllController@tampilmasukll')->middleware('loginNaradaAuth');
Route::get('/addmasukll','MasukllController@add')->middleware('loginNaradaAuth');
Route::post('/addedmasukll','MasukllController@added')->middleware('loginNaradaAuth');
Route::get('/editmasukll/{id}','MasukllController@edit')->middleware('loginNaradaAuth');
Route::post('/updatemasukll','MasukllController@update')->middleware('loginNaradaAuth');
Route::get('/hapusmasukll/{id}','MasukllController@hapus')->middleware('loginNaradaAuth');

Route::get('/lapmasukpg','LapmasukpgController@lapmasukpg')->name('laporan.lapmasukpg')->middleware('loginNaradaAuth');
Route::get('/lapgrafiktrip','LapgrafiktripController@lapgrafiktrip')->name('laporan.lapgrafiktrip')->middleware('loginNaradaAuth');
Route::get('/lapmasukposll','LapmasukposllController@lapmasukposll')->name('laporan.lapmasukposll')->middleware('loginNaradaAuth');
// ->middleware('akses.developer')

Route::get('/lapevaluasi','LapevaluasiController@lapevaluasi')->name('laporan.lapevaluasi')->middleware('loginNaradaAuth');
Route::get('/editeval/{id}','LapevaluasiController@edit')->middleware('loginNaradaAuth');
Route::post('/updateeval','LapevaluasiController@update')->middleware('loginNaradaAuth');

Route::get('/lapsemua','LapsemuaController@lapsemua')->name('laporan.lapsemua')->middleware('loginNaradaAuth');
Route::get('/lapkontrak','LapkontrakController@lapkontrak')->name('laporan.lapkontrak')->middleware('loginNaradaAuth');
Route::get('/laptransfer','LaptransferController@laptransfer')->name('laporan.laptransfer')->middleware('loginNaradaAuth');