<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    //
    protected $table = "group";
    protected $fillable = [
        'nama','credentials','create_at','update_at'
    ];
    public function employee()
    {
    	return $this->hasMany('App\Employee');
    }
    public function getEmployee()
    {
    	return $this->hasMany('App\Employee','nama_group');
    }
}
