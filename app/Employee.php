<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //
    protected $table = 'employee';
    protected $primaryKey = "id";
    //protected $fillable = [
        //'nama','foto','id_pg'
    //];
    protected $appends = [
        'nama_pg', 'nama_group'
    ];
    public function getNamaPgAttribute(){
        $data = Pg::find($this->id_pg);
        return $data->nama;
    }
    public function getNamaGroupAttribute(){
        $data = Group::find($this->id_group);
        return $data->nama;
    }
    public function group()
    {
    	return $this->belongsToMany('App\Group');
    }
    public function getPg()
    {
    	return $this->belongsTo('App\Pg', 'nama_pg', 'id');
    }
}
