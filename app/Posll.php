<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posll extends Model
{
    //
    protected $table = "posll";
    protected $fillable = [
        'kodepos','nama','alamat','asalpos','update_at'
    ];

    public function getWaktu()
    {
    	return $this->hasMany('App\Waktu','nama_posll');
    }
    public function getTrip()
    {
    	return $this->hasMany('App\Trip','nama_posll');
    }
}
