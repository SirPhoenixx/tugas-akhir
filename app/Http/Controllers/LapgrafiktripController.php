<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response;
use Illuminate\Support\Facades\DB;
use DateTime;
use App\Http\Requests;
use App\Pg;
use App\Posll;
use App\Trip;
use Carbon\Carbon;

class LapgrafiktripController extends Controller
{
    //
    public function lapgrafiktrip(Request $request) 
    {
        if($request->from_date>$request->to_date){
            $temp = $request->from_date;
            $request->from_date = $request->to_date;
            $request->to_date = $temp;
        }
        if($request->from_date!=null){ 
            $trip = Trip::whereBetween('tanggal_posll',[$request->from_date,$request->to_date])->get();
            $month_start = date("m",strtotime($request->from_date));
            $month_end = date("m",strtotime($request->to_date));
            $year_start = date("y",strtotime($request->from_date));
            $year_end = date("y",strtotime($request->to_date));
        }
        else 
        {
            $trip = Trip::All();
            $month_end = date("m",strtotime(Carbon::now()));
            $month_start = $month_end-2;
        }

        // dd($month_start);
        $pg = Pg::All();
        $posll = Posll::All();
        //data chart
        $categoriespg = [];
        $categoriesposll = [];
        $datapg = [];
        $dataposll = [];
        $month = [];
        $month_result = [];

        if ($month_start<=$month_end){
            for($i=$month_start;$i<=$month_end;$i++){
                $month[] = date("F", mktime(0,0,0,$i, 10));
                $month_result[] = Trip::whereMonth('tanggal_posll', '=', $i)->count();
            }
        }
        else {
            for($i=$month_start;$i<=12;$i++){
                $month[] = date("F", mktime(0,0,0,$i, 10));
                $month_result[] = Trip::whereMonth('tanggal_posll', '=', $i)->count();
            }
            for($i=1;$i<=$month_end;$i++){
                $month[] = date("F", mktime(0,0,0,$i, 10));
                $month_result[] = Trip::whereMonth('tanggal_posll', '=', $i)->count();
            }
        }
        // dd($month_result);
        foreach($pg as $p){
            $categoriespg[] = $p->nama;
        }
        foreach($posll as $r){
            $categoriesll[] = $r->nama;
        }

        foreach ($pg as $p){
            $datapg[] = $trip->where('id_pgmasuk',$p->id)->count();
        }
        foreach ($posll as $q){
            $dataposll[] = $trip->where('id_posll',$q->id)->count();
        }
        return view('laporan.lapgrafiktrip',[
            'trip' => $trip,
            'categoriespg'=>$categoriespg,
            'categoriesll'=>$categoriesll,
            'month'=>$month, 
            'datapg'=>$datapg,
            'month_result'=>$month_result,
            'dataposll'=>$dataposll 
        ]);
    }
}
