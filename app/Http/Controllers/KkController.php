<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Kk;
use App\Pg;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

class KkController extends Controller
{
    //
    public function __construct(Request $request)
    {
      $id_group = $request->session()->get('id_group'); // nyeluk role

      if ($id_group == '1' OR $id_group == '2')
      {
        return view('master.kk');
      }
      else
      {
        return abort(401);
      }
    }

    public function kk()
    {
      $kk = Kk::all();
        # code...
      return view('master.kk', ['kk' => $kk]);
    }
    public function tampilkk()
    {
      $kk = Kk::all();
      return Datatables::of($kk)
      ->addColumn('action', function ($kk) {
        return '
        <div class="row">
                        <div class="col-md-6">
        <a href="/editkk/'.$kk->id.'" class="btn btn-block btn-info btn-xs">Edit  <i class="fa fa-edit"></i></a> 
        </div>
        <div class="col-md-6">
        <form action="/hapuskk/'.$kk->id.'" method="GET"> '.csrf_field().' 
        <button type="submit" class="btn btn-block btn-danger btn-xs">Delete  <i class="fa fa-trash"></i></button></form></div></div>';
      })
        ->make(true);
    }
    public function add()
    {
      $pg = Pg::All();
      // memanggil view tambah
      return view('master.addkk', [
        'pg' => $pg
      ]);
    }

    public function added(Request $request)
    {
        // update data pegawai
        DB::table('kk')->insert([
            'nokontrak' => $request->inputnokontrak,
            'nama' => $request->inputnama,
            'id_pg' => $request->inputpg,
            'tahun' => $request->inputtahun
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/masterkk');
    }

    public function edit($id)
    {
    // mengambil data pegawai berdasarkan id yang dipilih
    $kk = DB::table('kk')->where('id',$id)->first();
    $pg = Pg::All();
    // passing data pegawai yang didapat ke view edit.blade.php
    return view('master.editkk',[
      'kk' => $kk,
      'pg' => $pg
      ]);
    }

    public function update(Request $request)
    {
        // update data pegawai
        DB::table('kk')->where('id',$request->inputid)->update([
            'nokontrak' => $request->inputnokontrak,
            'nama' => $request->inputnama,
            'id_pg' => $request->inputpg,
            'tahun' => $request->inputtahun
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/masterkk');
    }

    // method untuk hapus data pegawai
    public function hapus($id)
    {
        // menghapus data pegawai berdasarkan id yang dipilih
        DB::table('kk')->where('id',$id)->delete();
            
        // alihkan halaman ke halaman pegawai
        return redirect('/masterkk');
    }
}
