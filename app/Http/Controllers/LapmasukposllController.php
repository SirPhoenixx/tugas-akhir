<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Trip;
use App\Pg;
use App\Posll;

use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

class LapmasukposllController extends Controller
{
    //
    public function __construct(Request $request)
    {
      $id_group = $request->session()->get('id_group'); // nyeluk role

      if ($id_group == '3')
      {
          return abort(401);
        }
        else
        {
            return view('laporan.lapmasukposll');
      }
    }


    public function lapmasukposll() 
    {
        $trip = Trip::all();
        $pg = Pg::all();
        $posll = Posll::all();
        if(request()->ajax()) {
            //$query = Trip::query();
     
            $from_date = (!empty($_GET["from_date"])) ? ($_GET["from_date"]) : ('');
            $to_date = (!empty($_GET["to_date"])) ? ($_GET["to_date"]) : ('');
            $inputpg = (!empty($_GET["inputpg"])) ? ($_GET["inputpg"]) : ('');
            $inputposll = (!empty($_GET["inputposll"])) ? ($_GET["inputposll"]) : ('');
     
            if($from_date && $to_date){
         
             $from_date = date('Y-m-d', strtotime($from_date));
             $to_date = date('Y-m-d', strtotime($to_date));
              
             $trip = Trip::whereBetween('tanggal_posll', [$from_date,$to_date])->get();
            }
            if($inputpg){
                $trip = $trip->where('id_pgtujuan', $inputpg);

            }
            if($inputposll)
                $trip = $trip->where('id_posll', $inputposll);
                
            return datatables()->of($trip)
                ->make(true);
        }
        return view('laporan.lapmasukposll',[
            'trip' => $trip,
            'pg' => $pg,
            'posll' => $posll
        ]);
    }
}
