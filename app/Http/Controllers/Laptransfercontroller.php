<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Trip;
use Yajra\Datatables\Datatables;

class LaptransferController extends Controller
{

    public function laptransfer() 
    {
        $trip = Trip::where('status', '!=','Berhasil')->get();
        if(request()->ajax()) {

            
            // $query = Trip::query();
     
            $from_date = (!empty($_GET["from_date"])) ? ($_GET["from_date"]) : ('');
            $to_date = (!empty($_GET["to_date"])) ? ($_GET["to_date"]) : ('');
     
            if($from_date && $to_date){
         
             $from_date = date('Y-m-d', strtotime($from_date));
             $to_date = date('Y-m-d', strtotime($to_date));

             $query = Trip::whereBetween('tanggal_pg', [$from_date,$to_date])->get();
              
            //  $query->whereRaw("date(trip.tanggal_pg) >= '" . $from_date . "' AND date(trip.tanggal_pg) <= '" . $to_date . "'");
            }
            else $query = Trip::all();

            $trip = $query->where('status', '!=','Berhasil')->where('status', '!=', null);
            // $trip = $query->select('*');
            return datatables()->of($trip)
            ->addIndexColumn()
            ->make(true);
        }
        return view('laporan.laptransfer',[
            'trip' => $trip
        ]);
    }
}
