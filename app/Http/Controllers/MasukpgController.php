<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Trip;
use App\Pg;
use App\Posll;
use App\Kk;

use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

class MasukpgController extends Controller
{
    //
    public function __construct(Request $request)
    {
      $id_group = $request->session()->get('id_group'); // nyeluk role

      if ($id_group == '1' OR $id_group == '2' OR $id_group == '3')
      {
        return view('transaksi.masukpg');
      }
      else
      {
        return abort(401);
      }
    }

    public function masukpg()
    {
      $trip = Trip::all();
        # code...
      return view('transaksi.masukpg', ['trip' => $trip]);
    }
    public function tampilmasukpg()
    {
      $trip = Trip::all();
      return Datatables::of($trip)
      ->addColumn('action', function ($trip) {
        return '<a href="/editmasukpg/'.$trip->id.'" class="btn btn-block btn-info btn-xs">Edit  <i class="fa fa-edit"></i></a> 
        <form action="/hapusmasukpg/'.$trip->id.'" method="GET"> '.csrf_field().' 
        <button type="submit" class="btn btn-block btn-danger btn-xs">Delete  <i class="fa fa-trash"></i></button></form>';
      })
        ->make(true);
    }

    public function edit($id)
    {
    // mengambil data pegawai berdasarkan id yang dipilih
    $trip = DB::table('trip')->where('id',$id)->first();
    $pg = Pg::All();
    $posll = Posll::All();
    $kk = Kk::All();
    // passing data pegawai yang didapat ke view edit.blade.php
    return view('transaksi.editmasukpg',[
      'trip' => $trip,
      'posll' => $posll,
      'pg' => $pg,
      'kk' => $kk
      ]);
    }

    public function update(Request $request)
    {
        // update data pegawai
        
        if(Trip::where('id',$request->inputid)->first()->id_pgmasuk==null)
        Trip::where('id',$request->inputid)->update([
          'id_kontrak_pg' => $request->inputkontrakb,
          'plat_truk_pg' => $request->inputplatb,
          'tanggal_posll' => $request->inputtanggalposll,
          'tanggal_pg' => Carbon::now(),
          'petugas_pg' => $request->inputpetugaspg,
          'spta' => $request->inputspta,
          'keterangan' => $request->inputketerangan,
          'id_pgmasuk' => $request->inputpgmasuk,
          'status' => $request->inputstatus
        ]);
        else if(Trip::where('id',$request->inputid)->first()->id_pgmasuk!=null)
        Trip::where('id',$request->inputid)->update([
          'id_kontrak_pg' => $request->inputkontrakb,
          'plat_truk_pg' => $request->inputplatb,
          'tanggal_posll' => $request->inputtanggalposll,
          'petugas_pg' => $request->inputpetugaspg,
          'spta' => $request->inputspta,
          'keterangan' => $request->inputketerangan,
          'id_pgmasuk' => $request->inputpgmasuk,
          'status' => $request->inputstatus
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/transaksimasukpg');
    }

    // method untuk hapus data pegawai
    public function hapus($id)
    {
        // menghapus data pegawai berdasarkan id yang dipilih
        DB::table('trip')->where('id',$id)->delete();
            
        // alihkan halaman ke halaman pegawai
        return redirect('/transaksimasukpg');
    }
}
