<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Pg;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

class PgController extends Controller
{
    //
    public function __construct(Request $request)
    {
      $id_group = $request->session()->get('id_group'); // nyeluk role

      if ($id_group == '1' OR $id_group == '2')
      {
        return view('master.pg');
      }
      else
      {
        return abort(401);
      }
    }

    public function pg()
    {
      $pg = DB::table('pg');
        # code...
      return view('master.pg', ['pg' => $pg]);
    }

    public function tampilpg()
    {
      $pg = Pg::select(['id', 'kodepg', 'nama', 'alamat', 'emailpg', 'updated_at']);
      return Datatables::of($pg)
      ->addColumn('action', function ($pg) {
        return '
        <div class="row">
                        <div class="col-md-6">
        <a href="/editpg/'.$pg->id.'" class="btn btn-block btn-info btn-xs">Edit  <i class="fa fa-edit"></i></a> 
        </div>
        <div class="col-md-6">
        <form action="/hapuspg/'.$pg->id.'" method="GET"> '.csrf_field().' 
        <button type="submit" class="btn btn-block btn-danger btn-xs">Delete  <i class="fa fa-trash"></i></button></form></div></div>';
      })
        ->make(true);
    }
    public function add()
    {
      $pg = Pg::All();
      // memanggil view tambah
      return view('master.addpg', [
        'pg' => $pg
      ]);
    }

    public function added(Request $request)
    {
        // update data pegawai
        Pg::create([
            'kodepg' => $request->inputkodepg,
            'nama' => $request->inputnama,
            'alamat' => $request->inputlokasi,
            'emailpg' => $request->inputemailpg,
            'kota' => $request->inputkota,
            'unit' => $request->inputunit
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/masterpg');
    }

    public function edit($id)
    {
    // mengambil data pegawai berdasarkan id yang dipilih
    $pg = DB::table('pg')->where('id',$id)->get();
    // passing data pegawai yang didapat ke view edit.blade.php
    return view('master.editpg',['pg' => $pg]);
    }

    public function update(Request $request)
    {
        // update data pegawai
        Pg::where('id',$request->inputid)->update([
            'kodepg' => $request->inputkodepg,
            'nama' => $request->inputnama,
            'alamat' => $request->inputlokasi,
            'emailpg' => $request->inputemailpg,
            'kota' => $request->inputkota,
            'unit' => $request->inputunit
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/masterpg');
    }

    // method untuk hapus data pegawai
    public function hapus($id)
    {
        // menghapus data pegawai berdasarkan id yang dipilih
        DB::table('pg')->where('id',$id)->delete();
            
        // alihkan halaman ke halaman pegawai
        return redirect('/masterpg');
    }
}
