<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Employee;
use App\Group;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    //
    public function __construct(Request $request)
    {
      $id_group = $request->session()->get('id_group'); // nyeluk role

      if ($id_group == '1' OR $id_group == '2')
      {
        return view('manajemen.user');
      }
      else
      {
        return abort(401);
      }
    }

    public function employee()
    {
        $employee = DB::table('employee');
        # code...
      return view('manajemen.user', ['employee' => $employee]);
    }
    public function tampiluser()
    {
        $employee = Employee::all();
        return Datatables::of($employee)
        ->addColumn('action', function ($employee) {
            return '
            <div class="row">
                        <div class="col-md-6">
            <a href="/edituser/'.$employee->id.'" class="btn btn-block btn-info btn-xs">Edit  <i class="fa fa-edit"></i></a> 
            </div>
            <div class="col-md-6">
            <form action="/hapususer/'.$employee->id.'" method="GET"> '.csrf_field().' 
            <button type="submit" class="btn btn-block btn-danger btn-xs">Delete  <i class="fa fa-trash"></i></button></form></div></div>';
        })
        ->make(true);
    }

    public function edit($id)
    {
    // mengambil data pegawai berdasarkan id yang dipilih
    $employee = DB::table('employee')->where('id',$id)->first();
    $group = Group::All();
    // passing data pegawai yang didapat ke view edit.blade.php
    return view('manajemen.edituser',[
        'employee' => $employee,
        'group' => $group
        ]);
    }

    public function update(Request $request)
    {
        // update data pegawai
        DB::table('employee')->where('id',$request->inputid)->update([
            'username' => $request->inputusername,
            'email' => $request->inputemail,
            'nama' => $request->inputnama,
            'nomerhp' => $request->inputnomerhp,
            'id_group' => $request->inputgroup
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/manajemenuser');
    }

    // method untuk hapus data pegawai
    public function hapus($id)
    {
        // menghapus data pegawai berdasarkan id yang dipilih
        DB::table('employee')->where('id',$id)->delete();
            
        // alihkan halaman ke halaman pegawai
        return redirect('/manajemenuser');
    }
}
