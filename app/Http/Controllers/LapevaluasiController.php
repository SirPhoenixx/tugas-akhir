<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Trip;
use App\Pg;
use App\Kk;
use App\Posll;

use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

class LapevaluasiController extends Controller
{
    //
    public function __construct(Request $request)
    {
      $id_group = $request->session()->get('id_group'); // nyeluk role

      if ($id_group == '3' OR $id_group == '4')
      {
          return abort(401);
        }
        else
        {
            return view('laporan.lapevaluasi');
      }
    }

    public function lapevaluasi() 
    {
        $trip = Trip::all();
        $pg = Pg::all();
        $posll = Posll::all();
        // $nopolll = Trip::find($this->plat_truk_ll);
        // $nopolpg = Trip::find($this->plat_truk_pg);
        // // dd($evalnopol);
        if(request()->ajax()) {

            
            // $query = Trip::query();
     
            $from_date = (!empty($_GET["from_date"])) ? ($_GET["from_date"]) : ('');
            $to_date = (!empty($_GET["to_date"])) ? ($_GET["to_date"]) : ('');
     
            if($from_date && $to_date){
         
             $from_date = date('Y-m-d', strtotime($from_date));
             $to_date = date('Y-m-d', strtotime($to_date));
              
             $trip = Trip::whereBetween('tanggal_pg', [$from_date,$to_date])->get(); 
            }
            // $trip = $query->select('*');
            return datatables()->of($trip)
            ->addColumn('action', function ($trip) {
                return '
                <a href="/editeval/'.$trip->id.'" class="btn btn-block btn-danger btn-xs">Eval  <i class="fa fa-edit"></i></a>';
            })
            ->addIndexColumn()
            ->make(true);
        }
        return view('laporan.lapevaluasi',[
            'trip' => $trip
        ]);
    }
    public function edit($id)
    {
    // mengambil data pegawai berdasarkan id yang dipilih
    $trip = DB::table('trip')->where('id',$id)->first();
    $pg = Pg::All();
    $posll = Posll::All();
    $kk = Kk::All();
    // passing data pegawai yang didapat ke view edit.blade.php
    return view('laporan.eval',[
      'trip' => $trip,
      'posll' => $posll,
      'pg' => $pg,
      'kk' => $kk
      ]);
    }

    public function update(Request $request)
    {
        // update data pegawai
        Trip::where('id',$request->inputid)->update([
            'eval_plat_truk' => $request->inputevalnopol,
            'eval_no_kontrak' => $request->inputevalnokontrak,
            'eval_waktu' => $request->inputevalwaktu,
            'tanggal_eval' => Carbon::now(),
            'hasil_eval' => $request->inputhasileval
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/lapevaluasi');
    }
}
