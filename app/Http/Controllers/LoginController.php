<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Auth;

class LoginController extends Controller
{
    //
    public function index(){
        if(!Session::get('login')){
            return redirect('login')->with('alert','Kamu harus login dulu');
        }
        else{
            return view('layout.dashboard');
        }
    }

    public function login(){
        return view('layout.login');
    }

    public function validasi(Request $request){

        $username = $request->username;
        $password = $request->password;

        $data = Employee::where('username',$username)->first();
        if($data){ //apakah email tersebut ada atau tidak
            if(Hash::check($password,$data->password)){
                Session::put('nama',$data->nama);
                Session::put('id_group',$data->id_group);
                Session::put('username',$data->username);
                Session::put('login',TRUE);
                return redirect('/dash');
            }
            else{
                return redirect('/login')->with('alert','Password salah !');
            }
        }
        else{
            return redirect('/login')->with('alert','Email tidak terdaftar!');
        }
    }

    public function logout(){
        Session::flush();
        return redirect('/login')->with('alert','Kamu sudah logout');
    }
}