<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Trip;
use App\Pg;
use App\Posll;
use App\Kk;

use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

class MasukllController extends Controller
{
    //
    public function __construct(Request $request)
    {
      $id_group = $request->session()->get('id_group'); // nyeluk role

      if ($id_group == '1' OR $id_group == '2' OR $id_group == '4')
      {
        return view('transaksi.masukll');
      }
      else
      {
        return abort(401);
      }
    }

    public function masukll()
    {
      $trip = Trip::all();
        # code...
      return view('transaksi.masukll', ['trip' => $trip]);
    }
    public function tampilmasukll()
    {
      $trip = Trip::all();
      return Datatables::of($trip)
      ->addColumn('action', function ($trip) {
        // @if(trip->where("id",'.$trip->id.')->first()->'.$trip->id_pgmasuk.'==null)
        // @endif
        return '
        <div class="row">
        <div class="col-md-6">
        <a href="/editmasukll/'.$trip->id.'" class="btn btn-block btn-info btn-xs">Edit  <i class="fa fa-edit"></i></a> 
        </div>
        <div class="col-md-6">
        <form action="/hapusmasukll/'.$trip->id.'" method="GET"> '.csrf_field().' 
        <button type="submit" class="btn btn-block btn-danger btn-xs">Delete  <i class="fa fa-trash"></i></button></form></div></div>
        ';
      })
        ->make(true);
    }
    public function add()
    {
      $trip = Trip::All();
      $pg = Pg::All();
      $posll = Posll::All();
      $kk = Kk::All();
      // memanggil view tambah
      return view('transaksi.addmasukll', [
        'trip' => $trip,
        'posll' => $posll,
        'pg' => $pg,
        'kk' => $kk
      ]);
    }
    public function added(Request $request)
    {
      // dd($request->inputplat);
        // update data pegawai
        Trip::create([
            'plat_truk_ll' => $request->inputplat,
            'id_kontrak_ll' => $request->inputkontrak,
            'tanggal_posll' => Carbon::now(),
            'petugas_ll' => $request->inputpetugasll,
            'kode_hitung' => $request->inputkode,
            'id_posll' => $request->inputposll,
            'id_pgtujuan' => $request->inputpgtujuan
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/transaksimasukll');
    }

    public function edit($id)
    {
    // mengambil data pegawai berdasarkan id yang dipilih
    $trip = DB::table('trip')->where('id',$id)->first();
    $pg = Pg::All();
    $posll = Posll::All();
    $kk = Kk::All();
    // passing data pegawai yang didapat ke view edit.blade.php
    return view('transaksi.editmasukll',[
      'trip' => $trip,
      'posll' => $posll,
      'pg' => $pg,
      'kk' => $kk
      ]);
    }

    public function update(Request $request)
    {
        // update data pegawai
        Trip::where('id',$request->inputid)->update([
          'plat_truk_ll' => $request->inputplat,
          'id_kontrak_ll' => $request->inputkontrak,
          'petugas_ll' => $request->inputpetugasll,
          'kode_hitung' => $request->inputkode,
          'id_posll' => $request->inputposll,
          'id_pgtujuan' => $request->inputpgtujuan
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/transaksimasukll');
    }

    // method untuk hapus data pegawai
    public function hapus($id)
    {
        // menghapus data pegawai berdasarkan id yang dipilih
        DB::table('trip')->where('id',$id)->delete();
            
        // alihkan halaman ke halaman pegawai
        return redirect('/transaksimasukll');
    }
}
