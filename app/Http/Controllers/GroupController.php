<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Group;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

class GroupController extends Controller
{
    //
    public function __construct(Request $request)
    {
      $id_group = $request->session()->get('id_group'); // nyeluk role

      if ($id_group == '1' OR $id_group == '2')
      {
        return view('manajemen.group');
      }
      else
      {
        return abort(401);
      }
    }

    public function group()
    {
        $group = DB::table('group');
        # code...
      return view('manajemen.group', ['group' => $group]);
    }
    public function tampilgroup()
    {
      $group = Group::select(['id', 'nama', 'created_at', 'updated_at', 'credentials']);
      return Datatables::of($group)
      ->addColumn('action', function ($group) {
        return '
        <div class="row">
                        <div class="col-md-6">
        <a href="/editgroup/'.$group->id.'" class="btn btn-block btn-info btn-xs">Edit  <i class="fa fa-edit"></i></a>
        </div> 
        <div class="col-md-6">
        <form action="/hapusgroup/'.$group->id.'" method="GET"> '.csrf_field().' 
        <button type="submit" class="btn btn-block btn-danger btn-xs">Delete  <i class="fa fa-trash"></i></button></form></div></div>';
      })
        ->make(true);
    }
    public function add()
    {
      $group = Group::All();
      // memanggil view tambah
      return view('manajemen.addgroup', [
        'group' => $group
      ]);
    }
    public function added(Request $request)
    {
        // update data pegawai
        Group::create([
            'nama' => $request->inputnama,
            'credentials' => $request->inputcredentials
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/manajemengroup');
    }
    public function edit($id)
    {
    // mengambil data pegawai berdasarkan id yang dipilih
    $group = DB::table('group')->where('id',$id)->get();
    // passing data pegawai yang didapat ke view edit.blade.php
    return view('manajemen.editgroup',['group' => $group]);
    }
    public function update(Request $request)
    {
        // update data pegawai
        Group::where('id',$request->inputid)->update([
            'nama' => $request->inputnama,
            'credentials' => $request->inputcredentials
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/manajemengroup');
    }
    public function hapus($id)
    {
        // menghapus data pegawai berdasarkan id yang dipilih
        DB::table('group')->where('id',$id)->delete();
            
        // alihkan halaman ke halaman pegawai
        return redirect('/manajemengroup');
    }
}
