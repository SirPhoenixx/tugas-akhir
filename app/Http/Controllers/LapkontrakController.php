<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Trip;
use App\Kk;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;

class LapkontrakController extends Controller
{
    //
    public function lapkontrak() 
    {
        $trip = Trip::All();
        $kk = Kk::All();
        //tambahane
        $categorieskk = [];
        $datakk = [];
        $todaykk = [];
        $sdtodaykk = [];
        $nokontrak = [];

        foreach($kk as $p){
            $categorieskk[] = $p->nokontrak;
            $nama[$p->nokontrak] = $p->nama;
            $sdtodaykk[$p->nokontrak] = Trip::where('id_kontrak_ll',$p->id)->count();
            $todaykk[$p->nokontrak] = Trip::where('id_kontrak_ll',$p->id)
            ->whereDate('tanggal_posll', Carbon::today())
            ->where('id_kontrak_ll','!=',null)->count(); 
        }
        // dd($categorieskk);

        if(request()->ajax()) {

            
            $query = Trip::query();
     
            $from_date = (!empty($_GET["from_date"])) ? ($_GET["from_date"]) : ('');
            $to_date = (!empty($_GET["to_date"])) ? ($_GET["to_date"]) : ('');
     
            if($from_date && $to_date){
         
             $from_date = date('Y-m-d', strtotime($from_date));
             $to_date = date('Y-m-d', strtotime($to_date));
              
             $query->whereRaw("date(trip.tanggal_posll) >= '" . $from_date . "' AND date(trip.tanggal_posll) <= '" . $to_date . "'");
            }
            $trip = $query->select('*');
            return datatables()->of($trip)
            ->addIndexColumn()
            ->make(true);
        }
        return view('laporan.lapkontrak',[
            'trip' => $trip,
            'todaykk'=>$todaykk,
            'sdtodaykk'=>$sdtodaykk,
            'nama'=>$nama,
            'categorieskk'=>$categorieskk
        ]);
    }
}
