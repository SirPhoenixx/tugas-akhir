<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Pg;
use App\Posll;
use App\Trip;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;

class DashboardController extends Controller
{
    //
    public function dashboard(Request $request)
    {
        $pg = Pg::All();
        $posll = Posll::All();
        $trip = Trip::All();
        //data chart
        
        // if($request->inputtahun!=null){ 
        //     $trip = Trip::whereBetween('tanggal_posll',[$request->inputtahun])->get();
        //     $inputtahun = date("y",strtotime($request->inputtahun));
        // }
        // else 
        // {
        //     $trip = Trip::All();
        //     $inputtahun = Carbon::now()->year;
        // }
        // // dd($inputtahun);
        $inputtahun = Carbon::now()->year;
        $categoriespg = [];
        $categoriesposll = [];
        $datapg = [];
        $dataposll = [];
        $sdtodaypg = [];
        $sdtodayposll = [];
        $todaypg = [];
        $todayposll = [];
        $years = range(2000, strftime("%Y", time()));
        //  dd($years);
        foreach($pg as $p){
            $categoriespg[] = $p->nama;
            $datapg[] = Trip::where('id_pgmasuk',$p->id)->count();
            $sdtodaypg[$p->nama] = Trip::where('id_pgmasuk',$p->id)->count();
            $todaypg[$p->nama] = Trip::where('id_pgmasuk',$p->id)
            ->whereDate('tanggal_pg', Carbon::today())
            ->where('id_pgmasuk','!=',null)->count(); 
        }
        foreach($posll as $r){
            $categoriesll[] = $r->nama;
            $dataposll[] = Trip::where('id_posll',$r->id)->count();
            $sdtodayposll[$r->nama] = Trip::where('id_posll',$r->id)->count();
            $todayposll[$r->nama] = Trip::where('id_posll',$r->id)
            ->whereDate('tanggal_posll', Carbon::today())
            ->where('id_posll','!=',null)->count(); 
        }
        // dd($todayposll);


        $trip_today = Trip::whereDate('tanggal_pg', Carbon::today())
        ->where('id_pgmasuk','!=',null)->count();
        $trip_all = Trip::where('id_pgmasuk','!=',null)->count();
        $tripposll_today = Trip::whereDate('created_at', Carbon::today())
        ->where('id_pgtujuan','!=',null)->count();
        // $tripposll_fix = $tripposll_today->where('id_posll','!=',null)->count();

        # code...
        return view('layout.dashboard',[
            'years'=>$years,
            'inputtahun'=>$inputtahun,
            'trip_today'=>$trip_today,
            'trip_all'=>$trip_all,
            'tripposll_today'=>$tripposll_today,
            'categoriespg'=>$categoriespg,
            'categoriesll'=>$categoriesll, 
            'datapg'=>$datapg,
            'dataposll'=>$dataposll,
            'todaypg'=>$todaypg,
            'sdtodaypg'=>$sdtodaypg,
            'todayposll'=>$todayposll,
            'sdtodayposll'=>$sdtodayposll
            ]);
    }
}
