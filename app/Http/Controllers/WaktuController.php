<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Waktu;
use App\Pg;
use App\Posll;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

class WaktuController extends Controller
{
    //
    public function __construct(Request $request)
    {
      $id_group = $request->session()->get('id_group'); // nyeluk role

      if ($id_group == '1' OR $id_group == '2' OR $id_group == '5')
      {
        return view('master.waktu');
      }
      else
      {
        return abort(401);
      }
    }

    public function waktu()
    {
      $waktu = Waktu::all();
        # code...
      return view('master.waktu', ['waktu' => $waktu]);
    }
    public function tampilwaktu()
    {
      $waktu = Waktu::all();
      return Datatables::of($waktu)
      ->addColumn('action', function ($waktu) {
        return '
        <div class="row">
                        <div class="col-md-6">
        <a href="/editwaktu/'.$waktu->id.'" class="btn btn-block btn-info btn-xs">Edit  <i class="fa fa-edit"></i></a> 
        </div>
        <div class="col-md-6">
        <form action="/hapuswaktu/'.$waktu->id.'" method="GET"> '.csrf_field().' 
        <button type="submit" class="btn btn-block btn-danger btn-xs">Delete  <i class="fa fa-trash"></i></button></form></div></div>';
      })
        ->make(true);
    }
    public function add()
    {
      $pg = Pg::All();
      $posll = Posll::All();
      // memanggil view tambah
      return view('master.addwaktu', [
        'pg' => $pg,
        'posll' => $posll
      ]);
    }

    public function added(Request $request)
    {
        // update data pegawai
        DB::table('waktu')->insert([
            'id_pg' => $request->inputpg,
            'id_posll' => $request->inputposll,
            'nilai' => $request->inputnilai
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/masterwaktu');
    }

    public function edit($id)
    {
      //$waktu = Waktu::all();
      //return Datatables::of($waktu)
    // mengambil data pegawai berdasarkan id yang dipilih
    $waktu = DB::table('waktu')->where('id',$id)->first();
    $pg = Pg::All();
    $posll = Posll::All();
    // passing data pegawai yang didapat ke view edit.blade.php
    return view('master.editwaktu',[
      'waktu' => $waktu,
      'pg' => $pg,
      'posll' => $posll
      ]);
    }

    public function update(Request $request)
    {
        // update data pegawai
        DB::table('waktu')->where('id',$request->inputid)->update([
            'id_pg' => $request->inputpg,
            'id_posll' => $request->inputposll,
            'nilai' => $request->inputnilai
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/masterwaktu');
    }

    // method untuk hapus data pegawai
    public function hapus($id)
    {
        // menghapus data pegawai berdasarkan id yang dipilih
        DB::table('waktu')->where('id',$id)->delete();
            
        // alihkan halaman ke halaman pegawai
        return redirect('/masterwaktu');
    }
}
