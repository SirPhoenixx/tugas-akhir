<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Posll;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

class PosllController extends Controller
{
    //
    public function __construct(Request $request)
    {
      $id_group = $request->session()->get('id_group'); // nyeluk role

      if ($id_group == '1' OR $id_group == '2')
      {
        return view('master.posll');
      }
      else
      {
        return abort(401);
      }
    }

    public function posll()
    {
      $posll = DB::table('posll');
        # code...
      return view('master.posll', ['posll' => $posll]);
    }
    public function tampilposll()
    {
      $posll = Posll::select(['id', 'kodepos', 'nama', 'alamat', 'asalpos', 'updated_at']);
      return Datatables::of($posll)
      ->addColumn('action', function ($posll) {
        return '
        <div class="row">
                        <div class="col-md-6">
        <a href="/editposll/'.$posll->id.'" class="btn btn-block btn-info btn-xs">Edit  <i class="fa fa-edit"></i></a> 
        </div>
        <div class="col-md-6">
        <form action="/hapusposll/'.$posll->id.'" method="GET"> '.csrf_field().' 
        <button type="submit" class="btn btn-block btn-danger btn-xs">Delete  <i class="fa fa-trash"></i></button></form></div></div>';
      })
        ->make(true);
    }
    public function add()
    {
      // memanggil view tambah
      return view('master.addposll');
    }

    public function added(Request $request)
    {
        // update data pegawai
        Posll::create([
            'kodepos' => $request->inputkodeposll,
            'nama' => $request->inputnama,
            'alamat' => $request->inputlokasi,
            'kota' => $request->inputkota,
            'asalpos' => $request->inputasalpos
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/masterposll');
    }

    public function edit($id)
    {
    // mengambil data pegawai berdasarkan id yang dipilih
    $posll = DB::table('posll')->where('id',$id)->get();
    // passing data pegawai yang didapat ke view edit.blade.php
    return view('master.editposll',['posll' => $posll]);
    }

    public function update(Request $request)
    {
        // update data pegawai
        Posll::where('id',$request->inputid)->update([
            'kodepos' => $request->inputkodeposll,
            'nama' => $request->inputnama,
            'alamat' => $request->inputlokasi,
            'asalpos' => $request->inputasalpos,
            'kota' => $request->inputkota
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/masterposll');
    }

    // method untuk hapus data pegawai
    public function hapus($id)
    {
        // menghapus data pegawai berdasarkan id yang dipilih
        DB::table('posll')->where('id',$id)->delete();
            
        // alihkan halaman ke halaman pegawai
        return redirect('/masterposll');
    }
}
