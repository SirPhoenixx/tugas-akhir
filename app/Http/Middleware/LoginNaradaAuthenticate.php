<?php

namespace App\Http\Middleware;

use Closure;

class LoginNaradaAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->has('login')){
            return $next($request);
            // return redirect('/logout')->with('alert','Kamu harus login dulu');
        }
        else{
            return redirect('/login')->with('alert','Kamu harus login dulu');
            //dd('jancok');
        }
        
    }
}
