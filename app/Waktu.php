<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Waktu extends Model
{
    //
    protected $table = "waktu";
    protected $primaryKey = "id";
    //protected $fillable = [
    //    'id_pg','id_posll','nilai','create_at','update_at'
    //];
    protected $appends = [
        'nama_pg', 'nama_posll'
    ];
    public function getNamaPgAttribute(){
        $data = Pg::find($this->id_pg);
        return $data->nama;
    }
    public function getNamaPosllAttribute(){
        $data = Posll::find($this->id_posll);
        return $data->nama;
    }
    public function getPg()
    {
    	return $this->belongsTo('App\Pg', 'nama_pg', 'id');
    }
    public function getPosll()
    {
    	return $this->belongsTo('App\Posll', 'nama_posll', 'id');
    }
}