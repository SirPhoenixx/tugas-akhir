<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pg extends Model
{
    //
    protected $table = 'pg';
    protected $fillable = [
        'kodepg','nama','alamat','emailpg','kota','unit'
    ];
    public function getEmployee()
    {
    	return $this->hasMany('App\Employee','nama_pg');
    }
    public function getWaktu()
    {
    	return $this->hasMany('App\Waktu','nama_pg');
    }
    public function getKk()
    {
    	return $this->hasMany('App\Kk','nama_pg');
    }
    public function getTrip()
    {
        return $this->hasMany('App\Trip','nama_pga');
        return $this->hasMany('App\Trip','nama_pgb');
    }
}
