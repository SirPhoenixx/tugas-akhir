<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    //
    protected $table = "trip";
    protected $primaryKey = "id";
    protected $fillable = [
        'id_pgmasuk',
        'id_pgtujuan',
        'tanggal_posll', 
        'tanggal_pg', 
        'id_kontrak_ll',
        'id_kontrak_pg', 
        'id_posll', 
        'spta', 
        'petugas_ll', 
        'petugas_pg', 
        'keterangan', 
        'kode_hitung', 
        'status', 
        'plat_truk_ll',
        'plat_truk_pg',
        'eval_plat_truk',
        'eval_no_kontrak',
        'eval_waktu',
        'hasil_eval',
        'tanggal_eval'
    ];
    protected $appends = [
        'nama_pga', 'nama_pgb', 'nama_posll', 'nama_kka', 'nama_kkb'
    ];
    public function getNamaPgaAttribute(){
        $data = Pg::find($this->id_pgtujuan);
        return $data->nama;
    }
    public function getNamaPgbAttribute(){
        $data = Pg::find($this->id_pgmasuk);
        if ($data==null) return "Belum Sampai Ke Pabrik Gula";
        return $data->nama;
    }
    public function getNamaPosllAttribute(){
        $data = Posll::find($this->id_posll);
        return $data->nama;
    }
    public function getNamaKkaAttribute(){
        $data = Kk::find($this->id_kontrak_ll);
        return $data->nokontrak;
    }
    public function getNamaKkbAttribute(){
        $data = Kk::find($this->id_kontrak_pg);
        if ($data==null) return "";
        return $data->nokontrak;
    }
    public function getPg()
    {
        return $this->belongsTo('App\Pg', 'nama_pga', 'id');
        return $this->belongsTo('App\Pg', 'nama_pgb', 'id');
    }
    public function getPosll()
    {
    	return $this->belongsTo('App\Posll', 'nama_posll', 'id');
    }
    public function getKk()
    {
        return $this->belongsTo('App\Kk', 'nama_kka', 'id');
        return $this->belongsTo('App\Kk', 'nama_kkb', 'id');
    }
}
