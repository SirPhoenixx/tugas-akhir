<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kk extends Model
{
    //
    protected $table = "kk";
    protected $primarykey = "id";
    protected $fillable = [
        'id','nokontrak','nama','id_pg','tahun','create_at','update_at'
    ];
    protected $appends = [
        'nama_pg'
    ];
    public function getNamaPgAttribute(){
        $data = Pg::find($this->id_pg);
        return $data->nama;
    }
    public function getPg()
    {
    	return $this->belongsTo('App\Pg', 'nama_pg', 'id');
    }
    public function getTrip()
    {
        return $this->hasMany('App\Trip','nama_kka');
        return $this->hasMany('App\Trip','nama_kkb');
    }
}
